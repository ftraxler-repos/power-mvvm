module.exports = {
    title: 'PowerMVVM',
    description: 'A powerful, framework independent MVVM library with a strong focus on Commands',
    base: '/',
    dest: '../public',
    themeConfig: {
        search: false,
        nextLinks: false,
        prevLinks: false,
        nav: [
            { link: '/wiki/', text: 'Wiki' },
            { link: 'https://gitlab.com/ftraxler-repos/power-mvvm', text: 'Source' },
            { link: 'https://www.nuget.org/packages/PowerMVVM.Commands/', text: 'NuGet' }
        ],
        sidebar: {
            '/wiki/': [
                {
                  title: "Commands",
                  collapsable: false,
                  children: [
                    'commands/introduction',
                    'commands/usage',
                    'commands/statusprovider',
                    'commands/logging',
                    'commands/testability'
                  ]
                },
                {
                  title: "Core",
                  collapsable: false,
                  children: [
                    'core/introduction'
                  ]
                },
                {
                  title: "Exploring",
                  collapsable: false,
                  children: [
                    'preview-builds',
                    "demo"
                  ]
                }
              ]
          }
    },
    plugins: [
    ]
}
