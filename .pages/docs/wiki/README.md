# Power MVVM

A powerful, framework independent MVVM toolset.  

## PowerMVVM.Commands
The commanding library supports synchronous, as well as asynchronous commands.
It provides all the features you're expecting of a command!

::: tip
Version 1.0.6 is out now
:::

## PowerMVVM.Core
Currently **under development**, but it will provide basic view model functionality heavily based on PowerMVVM's Commands.
The purpose of this is to avoid issues with asynchronous magic setters.  
It is also planned to support plugins, to provide, e.g. RX support in the view models.
