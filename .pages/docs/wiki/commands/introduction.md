# Introduction
The commanding library supports synchronous, as well as asynchronous commands.
It provides all the features you're expecting of a command:
- preventing execution when already running
- handling asynchronous calls
- handling errors during command execution
- invalidating can execute when dependent upon a property
- calling continuation actions on the initial context
- configure await settings for the command actions
- showing and hiding loading indicators
- logging information when executing a command
- new command for long running CPU bound background tasks  
- an AutoResetCommand for fire-once purposes
- per command status providers

::: tip
Usage is simplified by providing factory methods on the static `Command` class.
It is possible to create generic, as well as non generic actions or Task based functions, with or without a `CanExecute` function.
For extensibility, it also provides a wrapper to run any already existing `ICommand`.
Factory methods for creating a singleton/lazy command are also available.
:::
