# Usage

## Standard Commands
```csharp
// command creation
Command.Create(Action action)
Command.Create(Action action, Func<bool> canExecute)
Command.Create<T>(Action<T> action)
Command.Create<T>(Action<T> action, Func<T, bool> canExecute)
Command.Create(Func<Task> action)
Command.Create<T>(Func<T, Task> action)
Command.Create(Func<Task> action, Func<bool> canExecute)
Command.Create<T>(Func<T, Task> action, Func<T, bool> canExecute)
Command.Create(ICommand command)

// command configuration
Command.Create(MyAction)
       .ContinueWith(MyContinuationAction)      // configure a continuation action
       .UseErrorHandler(MyErrorHandler)         // set an error handler
       .UseLoadingIndicator()                   // display the loading indicator (disabled by default)
       .UseLoadingIndicator(ILoadingIndicator)  // control an external loading indicator instance
       .UseLoadingIndicator(show, hide)         // control any loading states with show and hide callbacks
       .AllowMultipleExecution()                // disable the multi-execution prevention (enabled by default)
       .ContinueOncapturedContext()             // return to originating thread after command action execution (disabled by default)
       .ConfigureRaiseCanExecuteChanged(pco =>  // configure (or change) to automatically raise CanExecuteChanged
            pco.SetPropertyHolder(this)                         // when in the set view model
               .SubscribeToProperty(nameof(MyProperty))         // any of the subscribed properties change
       .AddRaiseCanExecuteChanged(pco =>        // configure to automatically raise CanExecuteChanged
            pco.SetPropertyHolder(anyViewModel)                         // when in one of the set view models
               .SubscribeToProperty(nameof(MyProperty))         // any of the subscribed properties change

// external services configuration
Command.LoggingProvider = (ILoggingProvider)provider;
Command.StatusProvider = (AStatusProvider)provider;
Command.AlwaysContinueOnCapturedContext = true;             // always return to the commands action context
```

## Special Commands

### Background Command
The special class `BackgroundCommand` supports long runing, CPU bound tasks. It is best used for fire-and-forget implementations, such as triggering an upload.  
Cancellation, Errorhandling and Continuation Actions are supported as well. It also uses the standard logging provider (see above).  
`PowerMVVM.Commands.Testability` provides all necessary methods to test your command's code.  
*Please use with care, to avoid memory leaks and strong references to your view models when the command might run for a really long time!*

```csharp
// background task signature
Task MyBackgroundTask(object commandParameter, CancellationToken token) { ... }

// command configuration
new BackgroundCommand(MyBackgroundTask, Func<bool> canExecute, initialConcurrentRequests, maximumConcurrentRequests);
    .ContinueWith(Action<object> continuationAction)
    .RegisterCancellationTokenFactory(Func<CancellationToken> factory)
    .SetCancellationHandler(Action cancellationHandler)
    .SetErrorHandler(Action<Exception> errorHandler)
```

### Auto-Reset Command
The special class `AutoResetCommand` enables fire-once commands. Inspired by the `AutoResetEvent` of the framework, it just does nothing until it gets signalled.
When it's time to evaluate and execute the command, it triggers exactly once, and resets itself to a non-signalled state, waiting for the next signal to wake it up.  
Please note, that it will never block. It just gets ignored when not signalled.

```csharp
// command configuration
new AutoResetCommand(syncOrAsyncAction)                 // creates a non signalled auto reset command
    .ContinueWith(Action continuationAction)
    .SetErrorHandler(Action<Exception> errorHandler)
    
myAutoResetCommand.Signal();    \\ tell the command, that it should run exactly one time on it's next call to execute
```

## Customisation

### Extending PowerMVVM's commands
As mentioned before, it is possible to wrap any `ICommand` into a command, via the given factory method.
Still you can create your *own commands* by inheriting from the abstract base class `ACommand`.
