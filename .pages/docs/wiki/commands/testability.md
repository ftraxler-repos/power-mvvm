# Testability
For testability, the extension methods in `PowerMVVM.Commands.Testability` provide all necessary methods to test your internal implemetation. This allows to test *your* code, which means, that the test fails on errors in your code, instead of throwing an exception on the command.

```csharp
using PowerMVVM.Commands.Testability;

var canExecute = myCommand.CanExecute()
var canExecute = myCommand.CanExecute<T>(T param)
await myCommand.ExecuteActionAsync()
await myCommand.ExecuteActionAsync<T>(T param)
myCommand.ExecuteContinuationAction()
myCommand.ExecuteErrorHandler(Exception exception)
```
