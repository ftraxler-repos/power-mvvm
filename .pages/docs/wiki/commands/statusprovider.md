# Loading Indicators

## Status Informations on running commands
Implementing a loading spinner is quite easy with PowerMVVM. You have to inherit from `AStatusProvider` and implement your calls to the UI framework.
This allows to 
- show and hide a loding indicator

Register the status provider via the static property:
```csharp
Command.StatusProvider = new MyStatusProviderImplementation();
```

## Status Informations for separate commands
With Version 1.0.6 loading indicators can be set for each command differently, if necessary.
This provides more flexibility when showing, e.g. a loading indicator directly next to a button, while it might not be necessary to block the whole page, as the general indicator might do.

The `ACommand` exposes the following API for loading indicator customisation: 
```csharp
command.UseLoadingIndicator()                   // display the loading indicator (disabled by default)
       .UseLoadingIndicator(ILoadingIndicator)  // control an external loading indicator instance
       .UseLoadingIndicator(show, hide)         // control any loading states with show and hide callbacks
```

The `ILoadingIndicator` interface needs a proper implementation of the following methods for showing and hiding logic:
```csharp
Task ShowLoadingAsync();
Task HideLoadingAsync();
```
