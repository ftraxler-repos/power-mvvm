# Logging
You can use your preferred logging framework or method by creating a *logging provider* inheriting from `ILoggingProvider`. This allows to gain information about 
- when the command was called and whith which parameter
- the exception that occured, in case an exception occured
- when the command call completed

Register the LoggingProvider via the static property:
```csharp
Command.LoggingProvider = new MyLoggingProviderImplementation();
```
