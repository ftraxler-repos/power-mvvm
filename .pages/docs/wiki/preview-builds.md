# Preview Builds

It is possible to access preview versions of the framework.  
These are automatically generated when pushing into `develop` branch and published into the [GitLab Package Registry](https://gitlab.com/ftraxler-repos/power-mvvm/-/packages).

## Using PowerMVVM preview NuGet feed
Add the following NuGet feed to your Project:
```
https://gitlab.com/api/v4/projects/19193099/packages/nuget/index.json
```
Please make sure to enable *Preview Versions* for nugets in youd IDE.

::: tip
For detailed usage of GitLab's NuGet package registry please refer to the GitLab Docs[(https://docs.gitlab.com/ee/user/packages/nuget_repository/index.html)]
:::

### Add feed via dotnet CLI
Run the following command in your terminal for your project:
```
nuget source Add -Name "pMVVM-preview" -Source "https://gitlab.com/api/v4/projects/19193099/packages/nuget/index.json"
```

### Add feed via nuget.config
To use the package registry as a source for .NET create or adjust the file named `nuget.config` in the root of your project.  
Add or adapt it to provide the preview package source 
```xml
<?xml version="1.0" encoding="utf-8"?>
<configuration>
 <packageSources>
     <clear />
     <add key="pMVVM-preview" value="https://gitlab.com/api/v4/projects/19193099/packages/nuget/index.json" />
 </packageSources>
</configuration>
```
