---
home: true
# heroImage: /hero.png
actionText: Get Started →
actionLink: /wiki/
features:
- title: Lightweight
  details: The core framework builds fully on the .NET framework. No dependencies on libraries.
- title: Extensible
  details: Provides an API to simply extend the framework with your own implementations.
- title: Performant
  details: No reflection! Designed with performance in mind.
footer: MIT Licensed | Copyright © 2020-present Florian Traxler
---
