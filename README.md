# Power MVVM

A powerful, framework independent MVVM toolset.  
Released under the [MIT License](https://gitlab.com/ftraxler-repos/power-mvvm/-/blob/master/LICENSE)

For changelog please see [Releases](https://gitlab.com/ftraxler-repos/power-mvvm/-/releases)  
Find more information in the [Wiki](https://powermvvm.ftraxler.me/wiki)

## PowerMVVM.Commands
The commanding library supports synchronous, as well as asynchronous commands.
It provides all the features you're expecting of a command:
- preventing execution when already running
- handling asynchronous calls
- handling errors during command execution
- invalidating can execute when dependent upon a property
- calling continuation actions on the initial context
- configure await settings for the command actions
- showing and hiding loading indicators
- logging information when executing a command
- command for long running CPU bound background tasks  
- an AutoResetCommand for fire-once purposes
- new in **Version 1.0.6**: per command status providers

### Deprecations
- `StatusProvider.OnError(Exception ex)` will be deprecated in newer versions, because it is better practice to handle errors for every command instead of a catch-all error handling.

Previous deprecations:
- The usage of `BaseStatusProvider` is deprecated because of structural refactorings and renamings. Please refer to `AStatusProvider` instead!
- `ACommand.SetErrorHandler` is now `ACommand.UseErrorHandler`
- `ACommand.ConfigureRaiseCanExecuteChanged` is renamed to `ACommand.ConfigureRaiseCanExecuteChange`
- `AStatusProvider.ShowLoadingIndicatorInternal()` is deprecated because the async overload is preferred
- `AStatusProvider.HideLoadingIndicatorInternal()` is deprecated because the async overload is preferred


## PowerMVVM.Core
> coming soon, providing a simple, yet powerful ViewModel base!
