﻿using System;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows.Input;
using PowerMVVM.Commands.CommandSettings;

namespace PowerMVVM.Commands
{
    /// <summary>
    /// Factory class for creating different types of commands and configuring shared behaviors of every command type.
    /// Commands can be created every time, or lazy loaded and reused.
    /// </summary>
    public static class Command
    {
        private static ILoggingProvider loggingProvider = new DefaultLoggingProvider();
        private static AStatusProvider statusProvider = new NullStatusProvider();

        /// <summary>
        /// Configures all commands to return to their captured context after the command's action invocation.
        /// </summary>
        // ReSharper disable once UnusedAutoPropertyAccessor.Global - set by user if needed
        public static bool AlwaysContinueOnCapturedContext { get; set; } = false;

        /// <summary>
        /// Property holding the default or a custom <code>LoggingProvider</code>.
        /// </summary>
        public static ILoggingProvider LoggingProvider
        {
            get => loggingProvider;
            // ReSharper disable once ConstantNullCoalescingCondition - uses NullObjectPattern
            set => loggingProvider = value ?? new NullLoggingProvider();
        }

        /// <summary>
        /// Property holding the default or a custom <code>StatusProvider</code>.
        /// </summary>
        public static AStatusProvider StatusProvider
        {
            get => statusProvider;
            // ReSharper disable once ConstantNullCoalescingCondition - uses NullObjectPattern
            set => statusProvider = value ?? new NullStatusProvider();
        }

        /// <summary>
        /// Creates a new synchronous running command.
        /// </summary>
        public static ACommand Create(Action action, [CallerMemberName] string commandName = null!)
        {
            return new SynchronousCommand(action, commandName);
        }

        /// <summary>
        /// Creates a new synchronous running command with a custom <code>CanExecute</code> function.
        /// </summary>
        public static ACommand Create(Action action, Func<bool> canExecute, [CallerMemberName] string commandName = null!)
        {
            return new SynchronousCommand(action, canExecute, commandName);
        }

        /// <summary>
        /// Creates a new synchronous running command.
        /// </summary>
        public static ACommand Create<T>(Action<T> action, [CallerMemberName] string commandName = null!)
        {
            return new SynchronousCommandGeneric<T>(action, commandName);
        }

        /// <summary>
        /// Creates a new synchronous running command with a custom <code>CanExecute</code> function.
        /// </summary>
        public static ACommand Create<T>(Action<T> action, Func<T, bool> canExecute, [CallerMemberName] string commandName = null!)
        {
            return new SynchronousCommandGeneric<T>(action, canExecute, commandName);
        }

        /// <summary>
        /// Creates a new asynchronous running command.
        /// </summary>
        public static ACommand Create(Func<Task> action, [CallerMemberName] string commandName = null!)
        {
            return new AsynchronousCommand(action, commandName);
        }

        /// <summary>
        /// Creates a new asynchronous running command with a custom <code>CanExecute</code> function.
        /// </summary>
        public static ACommand Create<T>(Func<T, Task> action, [CallerMemberName] string commandName = null!)
        {
            return new AsynchronousCommandGeneric<T>(action, commandName);
        }

        /// <summary>
        /// Creates a new asynchronous running command.
        /// </summary>
        public static ACommand Create(Func<Task> action, Func<bool> canExecute, [CallerMemberName] string commandName = null!)
        {
            return new AsynchronousCommand(action, canExecute, commandName);
        }

        /// <summary>
        /// Creates a new asynchronous running command with a custom <code>CanExecute</code> function.
        /// </summary>
        public static ACommand Create<T>(Func<T, Task> action, Func<T, bool> canExecute, [CallerMemberName] string commandName = null!)
        {
            return new AsynchronousCommandGeneric<T>(action, canExecute, commandName);
        }

        /// <summary>
        /// Creates a new synchronous running command wrapping an <code>ICommand</code>.
        /// </summary>
        public static ACommand Create(ICommand command, [CallerMemberName] string commandName = null!)
        {
            return new WrappedCommand(command, commandName);
        }
    }
}