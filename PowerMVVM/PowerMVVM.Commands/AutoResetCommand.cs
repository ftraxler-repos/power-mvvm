﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;
using PowerMVVM.Commands.Testability;

namespace PowerMVVM.Commands
{
    public class AutoResetCommand : ICommand, ICommandExecutor
    {
        /// <inheritdoc />
        public event EventHandler? CanExecuteChanged;

        private readonly ACommand _command;
        private AutoResetFlag _flag;

        public AutoResetCommand(Action action)
        {
            _command = new SynchronousCommand(action ?? throw new ArgumentNullException(nameof(action)));
        }

        public AutoResetCommand(Func<Task> asyncAction)
        {
            _command = new AsynchronousCommand(asyncAction ?? throw new ArgumentNullException(nameof(asyncAction)));
        }

        private ICommand Command => _command;

        /// <inheritdoc />
        bool ICommand.CanExecute(object? parameter)
        {
            return CanExecuteInternal();
        }

        /// <inheritdoc />
        void ICommand.Execute(object parameter)
        {
            ExecuteInternal(parameter);
        }

        /// <inheritdoc />
        bool ICommandExecutor.CanExecute()
        {
            return CanExecuteInternal();
        }

        /// <inheritdoc />
        bool ICommandExecutor.CanExecute<T>(T parameter)
        {
            throw new CommandTestException();
        }

        /// <inheritdoc />
        Task ICommandExecutor.ExecuteActionAsync()
        {
            return _command.ExecuteActionAsync();
        }

        /// <inheritdoc />
        Task ICommandExecutor.ExecuteActionAsync<T>(T parameter)
        {
            throw new CommandTestException();
        }

        /// <inheritdoc />
        Task ICommandExecutor.ExecuteAsync(object? parameter)
        {
            ExecuteInternal(parameter);
            return Task.CompletedTask;
        }

        /// <inheritdoc />
        void ICommandExecutor.ExecuteContinuationAction()
        {
            _command.ExecuteContinuationAction();
        }

        /// <inheritdoc />
        void ICommandExecutor.ExecuteErrorHandler(Exception exception)
        {
            _command.ExecuteErrorHandler(exception);
        }

        /// <summary>
        /// Configures a <code>ContinuationAction</code> to be run after the command's action finished.
        /// The action is invoked on the command-creating thread, which mostly is the UI-Thread. This allows to easily trigger UI changes, although the command's logic was executed on another thread.
        /// </summary>
        /// <param name="continuationAction">The action to invoke after successful command execution.</param>
        public AutoResetCommand ContinueWith(Action continuationAction)
        {
            _command.ContinueWith(continuationAction ?? throw new ArgumentNullException(nameof(continuationAction)));
            return this;
        }

        /// <summary>
        /// Configures a callback, which get's invoked, when any <code>Exception</code> occured during the runtime of the command's logic.
        /// The callback is invoked on the command-creating thread, which mostly is the UI-Thread. This allows to easily trigger UI changes, although the command's logic was executed on another thread.
        /// </summary>
        /// <param name="errorHandler">The action to invoke when an exception gets caught.</param>
        public AutoResetCommand SetErrorHandler(Action<Exception> errorHandler)
        {
            _command.SetErrorHandler(errorHandler ?? throw new ArgumentNullException(nameof(errorHandler)));
            return this;
        }

        /// <summary>
        /// Signals the command, that it can run it's action when executing it the next time.
        /// When it is signalled to execute the command, the signal gets reset, igonring a sucessfull execution or a failure.
        /// </summary>
        public void Signal()
        {
            _flag.Signal();
            RaiseCanExecuteChanged();
        }

        private bool CanExecuteInternal()
        {
            return _flag.IsSignalled;
        }

        private void ExecuteInternal(object? parameter)
        {
            if (_flag.CheckSignalled())
            {
                Command.Execute(parameter);
                RaiseCanExecuteChanged();
            }
        }

        private void RaiseCanExecuteChanged()
        {
            CanExecuteChanged?.Invoke(this, EventArgs.Empty);
        }
    }
}