﻿using System;
using System.Threading.Tasks;

namespace PowerMVVM.Commands.CommandSettings
{
    internal interface IStatusProvider
    {
        Task HideLoadingIndicatorAsync(bool useLoading);
        
        Task ShowLoadingIndicatorAsync(bool useLoading);
    }
}