﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Threading.Tasks;

namespace PowerMVVM.Commands.CommandSettings
{
    [ExcludeFromCodeCoverage]
    internal sealed class NullStatusProvider : AStatusProvider
    {
        /// <inheritdoc />
        protected override Task HideLoadingIndicatorInternalAsync()
        {
            return Task.CompletedTask;
        }
        
        /// <inheritdoc />
        protected override Task ShowLoadingIndicatorInternalAsync()
        {
            return Task.CompletedTask;
        }
    }
}