﻿using System;
using System.Threading.Tasks;

namespace PowerMVVM.Commands.CommandSettings
{
    internal static class StatusProviderExtensions
    {
        public static Task HideLoadingIndicatorAsync(this IStatusProvider provider, bool useLoading)
        {
            return provider.HideLoadingIndicatorAsync(useLoading);
        }

        public static Task ShowLoadingIndicatorAsync(this IStatusProvider provider, bool useLoading)
        {
            return provider.ShowLoadingIndicatorAsync(useLoading);
        }
    }
}