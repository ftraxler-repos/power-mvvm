﻿using System;
using System.Diagnostics.CodeAnalysis;

namespace PowerMVVM.Commands.CommandSettings
{
    [ExcludeFromCodeCoverage]
    internal sealed class NullLoggingProvider : ILoggingProvider
    {
        /// <inheritdoc />
        public void OnCall(string commandName, object? parameter) { }

        /// <inheritdoc />
        public void OnCallCompleted(string commandName, object? parameter) { }

        /// <inheritdoc />
        public void OnException(Exception exception) { }
    }
}