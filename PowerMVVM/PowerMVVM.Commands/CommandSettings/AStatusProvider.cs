﻿using System.Threading.Tasks;

namespace PowerMVVM.Commands.CommandSettings
{
    /// <summary>
    /// Abstract class providing the base logic for a custom StatusProvider implementation for showing loading spinners.
    /// </summary>
    public abstract class AStatusProvider : IStatusProvider
    {
        Task IStatusProvider.HideLoadingIndicatorAsync(bool useLoading)
        {
            if (!useLoading)
                return Task.CompletedTask;
            
            return HideLoadingIndicatorInternalAsync();
        }

        Task IStatusProvider.ShowLoadingIndicatorAsync(bool useLoading)
        {
            if (!useLoading)
                return Task.CompletedTask;

            return ShowLoadingIndicatorInternalAsync();
        }

        protected abstract Task HideLoadingIndicatorInternalAsync();

        protected abstract Task ShowLoadingIndicatorInternalAsync();
    }
}