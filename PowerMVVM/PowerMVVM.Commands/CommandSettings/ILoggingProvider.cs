﻿using System;

namespace PowerMVVM.Commands.CommandSettings
{
    /// <summary>
    /// Interface defining logging hooks for command executions.
    /// </summary>
    public interface ILoggingProvider
    {
        /// <summary>
        /// This logging method gets called when a command starts it's execution.
        /// </summary>
        /// <param name="commandName">Provides the name of the command (or the name of the calling member).</param>
        /// <param name="parameter">Provides the passed parameter as <code>object</code>.</param>
        void OnCall(string commandName, object? parameter);

        /// <summary>
        /// This logging method gets called when a command has finished it's execution.
        /// </summary>
        /// <param name="commandName">Provides the name of the command (or the name of the calling member).</param>
        /// <param name="parameter">Provides the passed parameter as <code>object</code>.</param>
        void OnCallCompleted(string commandName, object? parameter);

        /// <summary>
        /// This logging method gets called when an exception occured during a command's execution.
        /// </summary>
        /// <param name="exception">The caught exception.</param>
        void OnException(Exception exception);
    }
}