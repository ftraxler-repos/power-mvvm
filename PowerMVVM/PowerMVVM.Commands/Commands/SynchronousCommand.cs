﻿using System;
using System.Threading.Tasks;

namespace PowerMVVM.Commands
{
    internal sealed class SynchronousCommand : ACommand
    {
        private readonly Action _action;
        private readonly Func<bool>? _canExecute;

        /// <inheritdoc />
        public SynchronousCommand(Action action,
                                  string commandName = nameof(SynchronousCommand)) : base(commandName)
        {
            _action = action ?? throw new ArgumentNullException(nameof(action));
            _canExecute = null;
        }

        /// <inheritdoc />
        public SynchronousCommand(Action action,
                                  Func<bool> canExecute,
                                  string commandName = nameof(SynchronousCommand)) : base(commandName)
        {
            _action = action ?? throw new ArgumentNullException(nameof(action));
            _canExecute = canExecute ?? throw new ArgumentNullException(nameof(canExecute));
        }

        /// <inheritdoc />
        protected override bool CanExecuteInternal(object? parameter)
        {
            return _canExecute == null || _canExecute();
        }

        /// <inheritdoc />
        protected override Task ExecuteInternalAsync(object? parameter)
        {
            _action();
            return Task.CompletedTask;
        }

        /// <inheritdoc />
        protected override bool GetConfigureAwait()
        {
            return true;
        }
    }
}