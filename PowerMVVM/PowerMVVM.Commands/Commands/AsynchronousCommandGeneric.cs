﻿using System;
using System.Threading.Tasks;

namespace PowerMVVM.Commands
{
    internal sealed class AsynchronousCommandGeneric<T> : ACommand
    {
        private readonly Func<T, Task> _action;
        private readonly Func<T, bool>? _canExecute;

        /// <inheritdoc />
        public AsynchronousCommandGeneric(Func<T, Task> action,
                                          string commandName = nameof(AsynchronousCommandGeneric<T>)) : base(commandName)
        {
            _action = action ?? throw new ArgumentNullException(nameof(action));
            _canExecute = null;
        }

        /// <inheritdoc />
        public AsynchronousCommandGeneric(Func<T, Task> action,
                                          Func<T, bool> canExecute,
                                          string commandName = nameof(AsynchronousCommandGeneric<T>)) : base(commandName)
        {
            _action = action ?? throw new ArgumentNullException(nameof(action));
            _canExecute = canExecute ?? throw new ArgumentNullException(nameof(canExecute));
        }

        /// <inheritdoc />
        protected override bool CanExecuteInternal(object? parameter)
        {
            return _canExecute == null || _canExecute(parameter.To<T>(_synchronisationContext));
        }

        /// <inheritdoc />
        protected override Task ExecuteInternalAsync(object? parameter)
        {
            return _action(parameter.To<T>(_synchronisationContext));
        }
    }
}