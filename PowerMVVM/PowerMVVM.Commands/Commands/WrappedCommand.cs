﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;

namespace PowerMVVM.Commands
{
    internal sealed class WrappedCommand : ACommand
    {
        private readonly ICommand _command;

        public WrappedCommand(ICommand command,
                              string commandName = nameof(WrappedCommand)) : base(commandName)
        {
            _command = command ?? throw new ArgumentNullException(nameof(command));
        }

        /// <inheritdoc />
        protected override bool CanExecuteInternal(object? parameter)
        {
            return _command.CanExecute(parameter);
        }

        /// <inheritdoc />
        protected override Task ExecuteInternalAsync(object? parameter)
        {
            _command.Execute(parameter);
            return Task.CompletedTask;
        }

        /// <inheritdoc />
        protected override bool GetConfigureAwait()
        {
            return true;
        }
    }
}