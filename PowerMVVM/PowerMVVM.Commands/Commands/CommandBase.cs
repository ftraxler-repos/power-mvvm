﻿using System;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;

namespace PowerMVVM.Commands
{
    /// <summary>
    /// The minimal command base for all commands
    /// (in fact the <code>ICommand</code> wrapper)
    /// </summary>
    public abstract class CommandBase : ICommand
    {
        /// <inheritdoc />
        public virtual event EventHandler CanExecuteChanged = null!;

        protected SynchronizationContext? _synchronisationContext;

        /// <inheritdoc />
        bool ICommand.CanExecute(object parameter)
        {
            return CanExecuteInternal(parameter);
        }

        /// <inheritdoc />
        async void ICommand.Execute(object parameter)
        {
            await ExecuteAsync(parameter);
        }

        protected abstract bool CanExecuteInternal(object? parameter);

        private protected abstract Task ExecuteAsync(object? parameter);

        /// <summary>
        /// Notify subscribers to invalidate the <code>CanExecute</code> value. 
        /// </summary>
        public void RaiseCanExecuteChanged()
        {
            TryInvokeOnMainContext(CanExecuteChanged, EventArgs.Empty);
        }

        // ReSharper disable once MemberCanBePrivate.Global
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private protected void TryInvokeOnMainContext(Action? action)
        {
            if (_synchronisationContext != null && _synchronisationContext != SynchronizationContext.Current)
                _synchronisationContext.Post(callback => action?.Invoke(), null);
            else
                action?.Invoke();
        }

        // ReSharper disable once MemberCanBePrivate.Global
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private protected void TryInvokeOnMainContext<T>(Action<T>? action, T parameter)
        {
            if (_synchronisationContext != null && _synchronisationContext != SynchronizationContext.Current)
                _synchronisationContext.Post(callback => action?.Invoke(parameter), null);
            else
                action?.Invoke(parameter);
        }

        // ReSharper disable once MemberCanBePrivate.Global
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private protected void TryInvokeOnMainContext(EventHandler? handler, EventArgs args)
        {
            if (_synchronisationContext != null && _synchronisationContext != SynchronizationContext.Current)
                _synchronisationContext.Post(callback => handler?.Invoke(this, args), null);
            else
                handler?.Invoke(this, args);
        }
    }
}