﻿using System;
using System.Threading.Tasks;

namespace PowerMVVM.Commands
{
    internal sealed class SynchronousCommandGeneric<T> : ACommand
    {
        private readonly Action<T> _action;
        private readonly Func<T, bool>? _canExecute;

        /// <inheritdoc />
        public SynchronousCommandGeneric(Action<T> action,
                                         string commandName = nameof(SynchronousCommandGeneric<T>)) : base(commandName)
        {
            _action = action ?? throw new ArgumentNullException(nameof(action));
            _canExecute = null;
        }

        /// <inheritdoc />
        public SynchronousCommandGeneric(Action<T> action,
                                         Func<T, bool> canExecute,
                                         string commandName = nameof(SynchronousCommandGeneric<T>)) : base(commandName)
        {
            _action = action ?? throw new ArgumentNullException(nameof(action));
            _canExecute = canExecute ?? throw new ArgumentNullException(nameof(canExecute));
        }

        /// <inheritdoc />
        protected override bool CanExecuteInternal(object? parameter)
        {
            return _canExecute == null || _canExecute(parameter.To<T>(_synchronisationContext));
        }

        /// <inheritdoc />
        protected override Task ExecuteInternalAsync(object? parameter)
        {
            _action(parameter.To<T>(_synchronisationContext));
            return Task.CompletedTask;
        }

        /// <inheritdoc />
        protected override bool GetConfigureAwait()
        {
            return true;
        }
    }
}