﻿using System;
using System.Threading.Tasks;

namespace PowerMVVM.Commands
{
    internal sealed class AsynchronousCommand : ACommand
    {
        private readonly Func<Task> _action;
        private readonly Func<bool>? _canExecute;

        /// <inheritdoc />
        public AsynchronousCommand(Func<Task> action,
                                   string commandName = nameof(AsynchronousCommand)) : base(commandName)
        {
            _action = action ?? throw new ArgumentNullException(nameof(action));
            _canExecute = null;
        }

        /// <inheritdoc />
        public AsynchronousCommand(Func<Task> action,
                                   Func<bool> canExecute,
                                   string commandName = nameof(AsynchronousCommand)) : base(commandName)
        {
            _action = action ?? throw new ArgumentNullException(nameof(action));
            _canExecute = canExecute ?? throw new ArgumentNullException(nameof(canExecute));
        }

        /// <inheritdoc />
        protected override bool CanExecuteInternal(object? parameter)
        {
            return _canExecute == null || _canExecute();
        }

        /// <inheritdoc />
        protected override Task ExecuteInternalAsync(object? parameter)
        {
            return _action();
        }
    }
}