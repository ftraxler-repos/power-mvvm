﻿using System;
using System.ComponentModel;

namespace PowerMVVM.Commands
{
    internal sealed class NotifyPropertyChangeEventWrapper
    {
        private readonly WeakReference<INotifyPropertyChangedListener> _weakEventListener;
        private INotifyPropertyChanged? _eventSource;

        public NotifyPropertyChangeEventWrapper(INotifyPropertyChangedListener eventListener)
        {
            _weakEventListener = new WeakReference<INotifyPropertyChangedListener>(eventListener);
        }

        public void RegisterEventSource(INotifyPropertyChanged eventSource)
        {
            if (_eventSource != null)
                _eventSource.PropertyChanged -= EventSourceOnPropertyChanged;
            _eventSource = eventSource;
            _eventSource.PropertyChanged += EventSourceOnPropertyChanged;
        }

        public void UnregisterEventSource()
        {
            if (_eventSource != null)
                _eventSource.PropertyChanged -= EventSourceOnPropertyChanged;
        }

        private void EventSourceOnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (_weakEventListener.TryGetTarget(out var eventListener))
                eventListener.OnPropertyChanged(sender, e);
            else
                UnregisterEventSource();
        }
    }
}