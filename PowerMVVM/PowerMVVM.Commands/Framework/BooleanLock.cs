﻿using System.Threading;

namespace PowerMVVM.Commands
{
    internal struct BooleanLock
    {
        private const int LOCKED = 1;
        private const int UNLOCKED = 0;

        public static BooleanLock CreateBreach()
        {
            return new BooleanLock(false, true);
        }

        private readonly bool _breached;
        private int _isLocked;

        public BooleanLock(bool isLocked)
        {
            _isLocked = isLocked ? LOCKED : UNLOCKED;
            _breached = false;
        }

        private BooleanLock(bool isLocked, bool breached) : this(isLocked)
        {
            _breached = breached;
        }

        public bool Lock()
        {
            if (_breached)
                return false;
            return Interlocked.Exchange(ref _isLocked, LOCKED) == LOCKED;
        }

        public void Free()
        {
            Interlocked.Exchange(ref _isLocked, UNLOCKED);
        }
    }
}