﻿using System.ComponentModel;

namespace PowerMVVM.Commands
{
    internal interface INotifyPropertyChangedListener
    {
        void OnPropertyChanged(object sender, PropertyChangedEventArgs e);
    }
}