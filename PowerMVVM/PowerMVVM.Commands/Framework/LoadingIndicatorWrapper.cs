﻿using System;
using System.Threading.Tasks;

namespace PowerMVVM.Commands
{
    internal class LoadingIndicatorWrapper : ILoadingIndicator
    {
        private readonly Func<Task> _hideTask;
        private readonly Func<Task> _showTask;

        public LoadingIndicatorWrapper(Func<Task> showTask, Func<Task> hideTask)
        {
            _showTask = showTask ?? throw new ArgumentNullException(nameof(showTask));
            _hideTask = hideTask ?? throw new ArgumentNullException(nameof(hideTask));
        }

        /// <inheritdoc />
        public Task HideLoadingAsync()
        {
            return _hideTask();
        }

        /// <inheritdoc />
        public Task ShowLoadingAsync(string commandName)
        {
            return _showTask();
        }
    }
}