﻿namespace PowerMVVM.Commands
{
    internal interface ICommandExceptionInvocator
    {
        void Invoke();
    }
}