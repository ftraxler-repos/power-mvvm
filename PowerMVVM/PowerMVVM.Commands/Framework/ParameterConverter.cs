﻿using System.Threading;

namespace PowerMVVM.Commands
{
    internal static class ParameterConverter
    {
        public static T To<T>(this object? parameter, SynchronizationContext? originatingContext)
        {
            if (parameter == null)
            {
                if (default(T) == null)
                    return (T)parameter!;
                InvokeParameterNullException<T>(originatingContext);
            }
            else
            {
                if (!(parameter is T))
                    InvokeInvalidCastException<T>(parameter!, originatingContext);
                else
                    return (T)parameter!;
            }
            return default!;
        }

        private static void InvokeInvalidCastException<TExpected>(object parameter, SynchronizationContext? originatingContext)
        {
            var exception = new CommandParameterCastException(typeof(TExpected), parameter.GetType(), originatingContext);
            Command.LoggingProvider.OnException(exception);
        }

        private static void InvokeParameterNullException<TExpected>(SynchronizationContext? originatingContext)
        {
            var exception = new CommandParameterNullException(typeof(TExpected), originatingContext);
            Command.LoggingProvider.OnException(exception);
        }
    }
}