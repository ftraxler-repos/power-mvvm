﻿using System;
using System.Threading;

namespace PowerMVVM.Commands
{
    public class CommandParameterCastException : CommandException
    {
        /// <inheritdoc />
        public CommandParameterCastException(Type expected, Type received, SynchronizationContext? originatingContext)
            : base(new InvalidCastException($"Expected Type '{expected}' but got Type '{received}' as a parameter"), originatingContext) { }
    }
}