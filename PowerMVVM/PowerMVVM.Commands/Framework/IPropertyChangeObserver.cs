﻿using System.ComponentModel;

namespace PowerMVVM.Commands
{
    /// <summary>
    /// This type allows to configure observers for properties on any <code>INotifyPropertyChanged</code>.
    /// It might feel a little clumsy, but it's powerful and fast!
    /// </summary>
    public interface IPropertyChangeObserver
    {
        /// <summary>
        /// Resets the observer to an unconfigured state.
        /// </summary>
        IPropertyChangeObserver Clear();

        /// <summary>
        /// Registers an instance of <code>INotifyPropertyChanged</code> to observe properties on.
        /// </summary>
        /// <param name="propertyHolder">Any view model or other class to observe.</param>
        IPropertyChangeObserver SetPropertyHolder(INotifyPropertyChanged propertyHolder);

        /// <summary>
        /// Subscribes to a property identified by it's name.
        /// </summary>
        /// <param name="propertyName">The name of the property to observe (case sensitive).</param>
        IPropertyChangeObserver SubscribeToProperty(string propertyName);

        /// <summary>
        /// Unsubscribes from an observed property.
        /// </summary>
        /// <param name="propertyName">The name of the property to stop observing (case sensitive).</param>
        IPropertyChangeObserver UnsubscribeProperty(string propertyName);
    }
}