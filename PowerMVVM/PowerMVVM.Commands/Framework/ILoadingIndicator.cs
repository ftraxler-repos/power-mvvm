﻿using System.Threading.Tasks;

namespace PowerMVVM.Commands
{
    /// <summary>
    /// Provides logic for a command to show and hide a loading indicator.
    /// </summary>
    public interface ILoadingIndicator
    {
        /// <summary>
        /// Gets called, when the command finishes execution.
        /// Should include logic to hide the loading indicator.
        /// </summary>
        public Task HideLoadingAsync();

        /// <summary>
        /// Gets called, when the command starts execution.
        /// Should include logic to show a loading indicator.
        /// A command name gets passed to the loading indicator to be able to identify the calling command.
        /// </summary>
        /// <param name="commandName">The name of the command that calls the loading indicator.</param>
        public Task ShowLoadingAsync(string commandName);
    }
}