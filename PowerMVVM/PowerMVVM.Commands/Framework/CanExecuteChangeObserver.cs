﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace PowerMVVM.Commands
{
    internal class CanExecuteChangeObserver : INotifyPropertyChangedListener, IPropertyChangeObserver
    {
        private readonly CommandBase _command;
        private readonly NotifyPropertyChangeEventWrapper _eventWrapper;
        private readonly HashSet<string> _properties;

        public CanExecuteChangeObserver(CommandBase command)
        {
            _command = command;
            _eventWrapper = new NotifyPropertyChangeEventWrapper(this);
            _properties = new HashSet<string>();
        }

        public void OnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (PropertiesContains(e.PropertyName))
                _command.RaiseCanExecuteChanged();
        }

        public IPropertyChangeObserver Clear()
        {
            _eventWrapper.UnregisterEventSource();
            _properties.Clear();
            return this;
        }

        public IPropertyChangeObserver SetPropertyHolder(INotifyPropertyChanged propertyHolder)
        {
            if (propertyHolder == null)
                throw new ArgumentNullException(nameof(propertyHolder));
            _eventWrapper.RegisterEventSource(propertyHolder);
            return this;
        }

        public IPropertyChangeObserver SubscribeToProperty(string propertyName)
        {
            if (string.IsNullOrEmpty(propertyName))
                throw new ArgumentNullException(nameof(propertyName));
            if (PropertiesContains(propertyName))
                throw new ArgumentException($"A property with name {propertyName} is already assigned!");
            PropertiesAdd(propertyName);
            return this;
        }

        public IPropertyChangeObserver UnsubscribeProperty(string propertyName)
        {
            if (string.IsNullOrEmpty(propertyName))
                throw new ArgumentNullException(nameof(propertyName));
            if (!PropertiesContains(propertyName))
                throw new ArgumentException($"A property with name {propertyName} was not assigned and therefore can not be removed!");
            PropertiesRemove(propertyName);
            return this;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private void PropertiesAdd(string value)
        {
            _properties.Add(value);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private bool PropertiesContains(string value)
        {
            return _properties.Contains(value);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private void PropertiesRemove(string value)
        {
            _properties.Remove(value);
        }
    }
}