﻿using System.Threading;

namespace PowerMVVM.Commands
{
    internal struct AutoResetFlag
    {
        private const int SIGNALLED = 1;
        private const int BLOKING = 0;
        private int _state;

        public void Signal()
        {
            Interlocked.Exchange(ref _state, SIGNALLED);
        }

        public bool CheckSignalled()
        {
            return Interlocked.Exchange(ref _state, BLOKING) == SIGNALLED;
        }

        public bool IsSignalled => _state == SIGNALLED;
    }
}