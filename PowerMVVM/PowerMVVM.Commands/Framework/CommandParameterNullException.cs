﻿using System;
using System.Threading;

namespace PowerMVVM.Commands
{
    public class CommandParameterNullException : CommandException
    {
        /// <inheritdoc />
        public CommandParameterNullException(Type expected, SynchronizationContext? originatingContext)
            : base(new InvalidCastException($"Expected a non nullable Type '{expected}' but got 'null' as a parameter"),
                originatingContext) { }
    }
}