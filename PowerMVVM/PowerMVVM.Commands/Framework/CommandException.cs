﻿using System;
using System.Threading;

namespace PowerMVVM.Commands
{
    /// <summary>
    /// A wrapper for any exception which might have occured during the execution of a command's action.
    /// </summary>
    public class CommandException : Exception, ICommandExceptionInvocator
    {
        private const string MESSAGE = "A PowerMVVM Command failed with an exception (see inner exception)";
        private readonly Exception _innerException;
        private readonly SynchronizationContext? _originatingContext;

        /// <inheritdoc />
        public CommandException(Exception innerException, SynchronizationContext? originatingContext) : base(MESSAGE, innerException)
        {
            _innerException = innerException ?? throw new ArgumentNullException(nameof(innerException));
            _originatingContext = originatingContext;
        }

        void ICommandExceptionInvocator.Invoke()
        {
            if (_originatingContext != null && _originatingContext != SynchronizationContext.Current)
                _originatingContext.Post(callback => throw _innerException, null);
            else
                throw _innerException;
        }
    }
}