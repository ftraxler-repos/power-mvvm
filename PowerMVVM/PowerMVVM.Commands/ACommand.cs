﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using PowerMVVM.Commands.CommandSettings;
using PowerMVVM.Commands.Testability;

namespace PowerMVVM.Commands
{
    /// <summary>
    /// This abstract class is holding different command implementations created via <code>Command.Create()</code> factory methods.
    /// It provides APIs to configure a command's behavior.
    /// </summary>
    public abstract class ACommand : CommandBase, ICommandExecutor
    {
        private readonly string _commandName;
        private IPropertyChangeObserver? _canExecuteChangeObserver;
        private bool _configureAwait;
        private Action? _continuationAction;
        private ILoadingIndicator? _customLoadingIndicator;
        private Action<Exception>? _errorHandler;
        private BooleanLock _lock;
        private List<IPropertyChangeObserver>? _trackedObservers;
        private bool _useLoadingIndicator;

        protected ACommand(string? commandName)
        {
            _commandName = commandName ?? nameof(ACommand);
            _lock = new BooleanLock();
            _synchronisationContext = SynchronizationContext.Current;
        }

        /// <inheritdoc />
        bool ICommandExecutor.CanExecute()
        {
            return CanExecuteInternal(null!);
        }

        /// <inheritdoc />
        bool ICommandExecutor.CanExecute<T>(T parameter)
        {
            return CanExecuteInternal(parameter);
        }

        /// <inheritdoc />
        Task ICommandExecutor.ExecuteActionAsync()
        {
            return ExecuteInternalAsync(null!);
        }

        /// <inheritdoc />
        Task ICommandExecutor.ExecuteActionAsync<T>(T parameter)
        {
            return ExecuteInternalAsync(parameter);
        }

        /// <inheritdoc />
        Task ICommandExecutor.ExecuteAsync(object? parameter)
        {
            return ExecuteAsync(parameter);
        }

        /// <inheritdoc />
        void ICommandExecutor.ExecuteContinuationAction()
        {
            if (_continuationAction == null)
                throw new ArgumentNullException(nameof(_continuationAction));
            _continuationAction.Invoke();
        }

        /// <inheritdoc />
        void ICommandExecutor.ExecuteErrorHandler(Exception exception)
        {
            if (_errorHandler == null)
                throw new ArgumentNullException(nameof(_errorHandler));
            if (exception == null)
                throw new ArgumentNullException(nameof(exception));
            _errorHandler.Invoke(exception);
        }

        protected abstract Task ExecuteInternalAsync(object? parameter);

        private protected override async Task ExecuteAsync(object? parameter)
        {
            if (_lock.Lock())
                return;

            var commandName = _commandName;
            Command.LoggingProvider.OnCall(commandName, parameter);
            await ShowLoadingIndicatorAsync();

            try
            {
                await ExecuteInternalAsync(parameter).ConfigureAwait(GetConfigureAwait());
                TryInvokeOnMainContext(_continuationAction);
            }
            catch (Exception ex)
            {
                Command.LoggingProvider.OnException(new CommandException(ex, _synchronisationContext));
                TryInvokeOnMainContext(_errorHandler, ex);
            }
            finally
            {
                await HideLoadingIndicatorAsync();
                Command.LoggingProvider.OnCallCompleted(commandName, parameter);
                _lock.Free();
            }
        }

        /// <summary>
        /// Creates a new <code>PropertyChangeObserver</code> instance, which allows to configure the command's <code>CanExecute</code> method to be revalidated when any observed property gets modified.
        /// In contrast to <see cref="ConfigureRaiseCanExecuteChanged" />, any added observer can not be removed or modified afterwards. But any amout of observers can be added.
        /// </summary>
        /// <param name="configuration">The <code>PropertyChangeObserver</code> instance to be configured for this command.</param>
        public ACommand AddRaiseCanExecuteChanged(Func<IPropertyChangeObserver, IPropertyChangeObserver> configuration)
        {
            AddObserver(configuration.Invoke(new CanExecuteChangeObserver(this)));
            return this;
        }

        /// <summary>
        /// Configures the Command to be executed again while it's action is currently running.
        /// </summary>
        public ACommand AllowMultipleExecution()
        {
            _lock = BooleanLock.CreateBreach();
            return this;
        }

        /// <summary>
        /// Provides a <code>PropertyChangeObserver</code> instance, which allows to configure the command's <code>CanExecute</code> method to be revalidated when any observed property gets modified.
        /// </summary>
        /// <param name="configuration">The <code>PropertyChangeObserver</code> instance to be configured for this command.</param>
        public ACommand ConfigureRaiseCanExecuteChanged(Func<IPropertyChangeObserver, IPropertyChangeObserver> configuration)
        {
            _canExecuteChangeObserver ??= new CanExecuteChangeObserver(this);
            _canExecuteChangeObserver = configuration.Invoke(_canExecuteChangeObserver);
            return this;
        }

        /// <summary>
        /// Configures the command's action to return to it's originating context after execution.
        /// </summary>
        public ACommand ContinueOncapturedContext()
        {
            _configureAwait = true;
            return this;
        }

        /// <summary>
        /// Configures a <code>ContinuationAction</code> to be run after the command's action finished.
        /// The action is invoked on the command-creating thread, which mostly is the UI-Thread. This allows to easily trigger UI changes, although the command's logic was executed on another thread.
        /// </summary>
        /// <param name="continuationAction">The action to invoke after successful command execution.</param>
        public ACommand ContinueWith(Action continuationAction)
        {
            _continuationAction = continuationAction ?? throw new ArgumentNullException(nameof(continuationAction));
            return this;
        }

        /// <summary>
        /// Configures a callback, which get's invoked, when any <code>Exception</code> occured during the runtime of the command's logic.
        /// The callback is invoked on the command-creating thread, which mostly is the UI-Thread. This allows to easily trigger UI changes, although the command's logic was executed on another thread.
        /// </summary>
        /// <param name="errorHandler">The action to invoke when an exception gets caught.</param>
        public ACommand SetErrorHandler(Action<Exception> errorHandler)
        {
            _errorHandler = errorHandler ?? throw new ArgumentNullException(nameof(errorHandler));
            return this;
        }

        /// <summary>
        /// Configures the command to show a loading indicator while it's logic is executed.
        /// The loading indicator invocation logic must be registered by assigning <code>Command.StatusProvider</code>.
        /// </summary>
        public ACommand UseLoadingIndicator()
        {
            _useLoadingIndicator = true;
            return this;
        }

        /// <summary>
        /// Configures the command to show a loading indicator while it's logic is executed.
        /// Any custom instance of <code>ILoadingIndicator</code> can be passed.
        /// </summary>
        /// <param name="indicator">The indicator logic containing class (e.g. view model).</param>
        public ACommand UseLoadingIndicator(ILoadingIndicator indicator)
        {
            _customLoadingIndicator = indicator ?? throw new ArgumentNullException(nameof(indicator));
            return this;
        }

        /// <summary>
        /// Configures the command to show a loading indicator while it's logic is executed.
        /// Takes to asynchronous function delegates, one for showing the indicator, one for hiding it.
        /// </summary>
        /// <param name="showIndicatorTask">The asynchronous function for showing the indicator.</param>
        /// <param name="hideIndicatorTask">The asynchronous function for hiding the indicator.</param>
        public ACommand UseLoadingIndicator(Func<Task> showIndicatorTask, Func<Task> hideIndicatorTask)
        {
            _customLoadingIndicator = new LoadingIndicatorWrapper(showIndicatorTask, hideIndicatorTask);
            return this;
        }

        protected virtual bool GetConfigureAwait()
        {
            return Command.AlwaysContinueOnCapturedContext || _configureAwait;
        }

        private void AddObserver(IPropertyChangeObserver observer)
        {
            if (_trackedObservers == null)
                _trackedObservers = new List<IPropertyChangeObserver>();
            _trackedObservers.Add(observer);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private Task HideLoadingIndicatorAsync()
        {
            return _customLoadingIndicator != null ? _customLoadingIndicator.HideLoadingAsync()
                       : Command.StatusProvider.HideLoadingIndicatorAsync(_useLoadingIndicator);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private Task ShowLoadingIndicatorAsync()
        {
            return _customLoadingIndicator != null ? _customLoadingIndicator.ShowLoadingAsync(_commandName)
                       : Command.StatusProvider.ShowLoadingIndicatorAsync(_useLoadingIndicator);
        }
    }
}