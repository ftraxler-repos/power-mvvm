using System;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using PowerMVVM.Commands.Testability;

namespace PowerMVVM.Commands
{
    /// <summary>
    /// This concrete class manages a command's logic, which is intended to run in background and provides APIs to configure it's behavior.
    /// It should be used for very long running tasks and especially for fire-and-forget commands such as triggering an upload. These actions are wrapped into a <code>Task.Factory.StartNew()</code> call.
    /// Please use with care to avoid strong references to ViewModels or other kinds of memory leaks!
    /// </summary>
    public sealed class BackgroundCommand : CommandBase, IBackgroundCommandExecutor
    {
        private readonly Func<object, CancellationToken, Task> _action;
        private readonly Func<object, bool> _canExecute;
        private readonly string _commandName;
        private readonly SemaphoreSlim _semaphore;
        private readonly int _semaphoreMaxCount;
        private Action? _cancellationHandler;
        private Func<CancellationToken>? _cancellationTokenFactory;
        private Action<object>? _continuationAction;
        private Action<Exception>? _errorHandler;

        public BackgroundCommand(Func<object, CancellationToken, Task> action) : this(action, o => true) { }

        /// <inheritdoc />
        public BackgroundCommand(Func<object, CancellationToken, Task> action,
                                 // ReSharper disable once IntroduceOptionalParameters.Global - force set both semaphore parameters
                                 Func<object, bool> canExecute) : this(action, canExecute, 1, 1) { }

        /// <inheritdoc />
        public BackgroundCommand(Func<object, CancellationToken, Task> action,
                                 Func<object, bool> canExecute,
                                 int initialConcurrentRequests,
                                 int maximumConcurrentRequests)
        {
            _action = action ?? throw new ArgumentNullException(nameof(action));
            _canExecute = canExecute ?? throw new ArgumentNullException(nameof(canExecute));
            _semaphore = new SemaphoreSlim(initialConcurrentRequests, maximumConcurrentRequests);
            _semaphoreMaxCount = maximumConcurrentRequests;
            _commandName = _action.Method.Name;
        }

        /// <param name="parameter"></param>
        /// <inheritdoc />
        bool IBackgroundCommandExecutor.CanExecute(object? parameter)
        {
            return CanExecuteInternal(parameter);
        }

        /// <inheritdoc />
        Task IBackgroundCommandExecutor.ExecuteActionAsync(CancellationToken cancellationToken, object? parameter)
        {
            return ActionExecutor(parameter, cancellationToken);
        }

        /// <inheritdoc />
        Task IBackgroundCommandExecutor.ExecuteAsync(object? parameter)
        {
            return ExecuteAsync(parameter);
        }

        /// <inheritdoc />
        void IBackgroundCommandExecutor.ExecuteCancellationAction()
        {
            if (_cancellationHandler == null)
                throw new ArgumentNullException(nameof(_cancellationHandler));
            _cancellationHandler.Invoke();
        }

        /// <inheritdoc />
        public CancellationToken ExecuteCancellationTokenFactoryMethod()
        {
            if (_cancellationTokenFactory == null)
                throw new ArgumentNullException(nameof(_cancellationTokenFactory));
            return _cancellationTokenFactory.Invoke();
        }

        /// <inheritdoc />
        void IBackgroundCommandExecutor.ExecuteContinuationAction(object? parameter)
        {
            if (_continuationAction == null)
                throw new ArgumentNullException(nameof(_continuationAction));
            _continuationAction.Invoke(parameter!);
        }

        /// <inheritdoc />
        void IBackgroundCommandExecutor.ExecuteErrorHandler(Exception exception)
        {
            if (_errorHandler == null)
                throw new ArgumentNullException(nameof(_errorHandler));
            if (exception == null)
                throw new ArgumentNullException(nameof(exception));
            _errorHandler.Invoke(exception);
        }

        /// <inheritdoc />
        protected override bool CanExecuteInternal(object? parameter)
        {
            return _canExecute(parameter!);
        }

        /// <inheritdoc />
        private protected override async Task ExecuteAsync(object? parameter)
        {
            Command.LoggingProvider.OnCall(_commandName, parameter);

            var cancellationToken = _cancellationTokenFactory?.Invoke() ?? CancellationToken.None;
            try
            {
                await _semaphore.WaitAsync(cancellationToken);
                await Task.Factory.StartNew(() => ActionExecutor(parameter, cancellationToken), cancellationToken);
            }
            catch (TaskCanceledException tex)
            {
                ReleaseSemaphore();
                Command.LoggingProvider.OnException(tex);
                TryInvokeOnMainContext(_cancellationHandler);
            }
            catch (Exception ex)
            {
                ReleaseSemaphore();
                Command.LoggingProvider.OnException(ex);
            }
        }

        /// <summary>
        /// Configures a <code>ContinuationAction</code> to be run after the command's action finished.
        /// The action is invoked on the command-creating thread, which mostly is the UI-Thread. This allows to easily trigger UI changes, although the command's logic was executed on another thread.
        /// </summary>
        /// <param name="continuationAction">The action to invoke after successful command execution.</param>
        public BackgroundCommand ContinueWith(Action<object> continuationAction)
        {
            _continuationAction = continuationAction ?? throw new ArgumentNullException(nameof(continuationAction));
            return this;
        }

        /// <summary>
        /// Registers a function, which gets called everytime the command is triggered, to receive a CancellationToken for the current execution. This allows to cancel multiple executions at different points in time.
        /// </summary>
        /// <param name="factory"></param>
        /// <returns></returns>
        public BackgroundCommand RegisterCancellationTokenFactory(Func<CancellationToken> factory)
        {
            _cancellationTokenFactory = factory ?? throw new ArgumentNullException(nameof(factory));
            return this;
        }

        /// <summary>
        /// Configures a callback, which get's invoked, when the background task got cancelled.
        /// The callback is invoked on the command-creating thread, which mostly is the UI-Thread. This allows to easily trigger UI changes, although the command's logic was executed on another thread.
        /// </summary>
        /// <param name="cancellationHandler">The action to invoke when the background execution gets cancelled.</param>
        /// <returns></returns>
        public BackgroundCommand SetCancellationHandler(Action cancellationHandler)
        {
            _cancellationHandler = cancellationHandler ?? throw new ArgumentNullException(nameof(cancellationHandler));
            return this;
        }

        /// <summary>
        /// Configures a callback, which get's invoked, when any <code>Exception</code> occured during the runtime of the command's logic.
        /// The callback is invoked on the command-creating thread, which mostly is the UI-Thread. This allows to easily trigger UI changes, although the command's logic was executed on another thread.
        /// </summary>
        /// <param name="errorHandler">The action to invoke when an exception gets caught.</param>
        public BackgroundCommand SetErrorHandler(Action<Exception> errorHandler)
        {
            _errorHandler = errorHandler ?? throw new ArgumentNullException(nameof(errorHandler));
            return this;
        }

        private async Task ActionExecutor(object? parameter, CancellationToken cancellationToken)
        {
            try
            {
                await _action(parameter!, cancellationToken);
                TryInvokeOnMainContext(_continuationAction, parameter!);
            }
            catch (TaskCanceledException tex)
            {
                Command.LoggingProvider.OnException(tex);
                TryInvokeOnMainContext(_cancellationHandler);
            }
            catch (Exception ex)
            {
                Command.LoggingProvider.OnException(new CommandException(ex, _synchronisationContext));
                TryInvokeOnMainContext(_errorHandler, ex);
            }
            finally
            {
                ReleaseSemaphore();
                Command.LoggingProvider.OnCallCompleted(_commandName, parameter);
            }
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private void ReleaseSemaphore()
        {
            if (_semaphore.CurrentCount < _semaphoreMaxCount)
                _semaphore.Release();
        }
    }
}