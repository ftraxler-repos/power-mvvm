﻿using System;
using System.Threading.Tasks;

namespace PowerMVVM.Commands.Testability
{
    /// <summary>
    /// Class holding extension methods for Command testability.
    /// </summary>
    public static class CommandExecutorExtensions
    {
        /// <summary>
        /// Validates the CanExecute function of the command.
        /// </summary>
        /// <param name="command">Should be the system under test.</param>
        /// <returns>Value determined by the invokation of the provided CanExecute function.</returns>
        public static bool CanExecute(this ICommandExecutor command)
        {
            return command.CanExecute();
        }

        /// <summary>
        /// Validates the CanExecute function of the command.
        /// </summary>
        /// <param name="command">Should be the system under test.</param>
        /// <param name="parameter">Command parameter to validate the CanExecute function with.</param>
        /// <returns>Value determined by the invokation of the provided CanExecute function.</returns>
        public static bool CanExecute<T>(this ICommandExecutor command, T parameter)
        {
            return command.CanExecute(parameter);
        }

        /// <summary>
        /// Executes the commands inner action.
        /// </summary>
        /// <param name="command">Should be the system under test.</param>
        /// <returns></returns>
        public static Task ExecuteActionAsync(this ICommandExecutor command)
        {
            return command.ExecuteActionAsync();
        }

        /// <summary>
        /// Executes the commands inner action.
        /// </summary>
        /// <param name="command">Should be the system under test.</param>
        /// <param name="parameter">Command parameter to be passed to the commands action.</param>
        /// <returns></returns>
        public static Task ExecuteActionAsync<T>(this ICommandExecutor command, T parameter)
        {
            return command.ExecuteActionAsync(parameter);
        }

        /// <summary>
        /// Executes the commands ContinuatiionAction.
        /// </summary>
        /// <param name="command">Should be the system under test.</param>
        public static void ExecuteContinuationAction(this ICommandExecutor command)
        {
            command.ExecuteContinuationAction();
        }

        /// <summary>
        /// Executes the commands ErrorHandler.
        /// </summary>
        /// <param name="command">Should be the system under test.</param>
        /// <param name="exception">Exception to be passed to the error callback.</param>
        public static void ExecuteErrorHandler(this ICommandExecutor command, Exception exception)
        {
            command.ExecuteErrorHandler(exception);
        }
    }
}