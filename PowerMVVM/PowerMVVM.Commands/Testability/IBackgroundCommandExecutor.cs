﻿using System;
using System.Threading;
using System.Threading.Tasks;
// ReSharper disable MissingXmlDoc - used explicitely via extension methods but must be public (due to extension methods)

namespace PowerMVVM.Commands.Testability
{
    public interface IBackgroundCommandExecutor
    {
        bool CanExecute(object? parameter = null);

        Task ExecuteActionAsync(CancellationToken cancellationToken, object? parameter = null);

        Task ExecuteAsync(object? parameter = null);

        void ExecuteCancellationAction();

        void ExecuteContinuationAction(object? parameter = null);

        void ExecuteErrorHandler(Exception exception);

        CancellationToken ExecuteCancellationTokenFactoryMethod();
    }
}