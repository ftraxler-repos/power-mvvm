﻿using System;

namespace PowerMVVM.Commands.Testability
{
    public class CommandTestException : InvalidOperationException
    {
        private const string EXCEPTION_MESSAGE =
            "The executed testability method is not allowed for this command, because the command does not support this type of operation.";

        public CommandTestException() : base(EXCEPTION_MESSAGE) { }
    }
}