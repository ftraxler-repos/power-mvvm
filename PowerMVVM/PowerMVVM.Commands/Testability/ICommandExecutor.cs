﻿using System;
using System.Threading.Tasks;
// ReSharper disable MissingXmlDoc - used explicitely via extension methods but must be public (due to extension methods)

namespace PowerMVVM.Commands.Testability
{
    public interface ICommandExecutor
    {
        bool CanExecute();

        bool CanExecute<T>(T parameter);

        Task ExecuteActionAsync();

        Task ExecuteActionAsync<T>(T parameter);

        Task ExecuteAsync(object? parameter);

        void ExecuteContinuationAction();

        void ExecuteErrorHandler(Exception exception);
    }
}