﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace PowerMVVM.Commands.Testability
{
    /// <summary>
    /// Class holding extension methods for Background Command testability.
    /// </summary>
    public static class BackgroundCommandExecutorExtensions
    {
        /// <summary>
        /// Validates the CanExecute function of the command.
        /// </summary>
        /// <param name="command">Should be the system under test.</param>
        /// <param name="parameter">Command parameter to validate the CanExecute function with.</param>
        /// <returns>Value determined by the invokation of the provided CanExecute function.</returns>
        public static bool CanExecute(this IBackgroundCommandExecutor command, object? parameter = null)
        {
            return command.CanExecute(parameter!);
        }

        /// <summary>
        /// Executes the commands inner action.
        /// </summary>
        /// <param name="command">Should be the system under test.</param>
        /// <param name="cancellationToken">A cancellation token for the execution.</param>
        /// <param name="parameter">Command parameter to be passed to the commands action.</param>
        public static Task ExecuteActionAsync(this IBackgroundCommandExecutor command,
                                              CancellationToken cancellationToken,
                                              object? parameter = null)
        {
            return command.ExecuteActionAsync(cancellationToken, parameter);
        }

        /// <summary>
        /// Executes the commands callback which would be called in case the command got cancelled.
        /// </summary>
        /// <param name="command">Should be the system under test.</param>
        public static void ExecuteCancellationAction(this IBackgroundCommandExecutor command)
        {
            command.ExecuteCancellationAction();
        }

        /// <summary>
        /// Executes the cancellation token factory method and returns a cancellation token.
        /// Used to validate logic to handle multiple parallel executions which need multiple cancellation tokens.
        /// </summary>
        /// <param name="command">Should be the system under test.</param>
        /// <returns>Cancellation token provided by a factory call.</returns>
        public static CancellationToken ExecuteCancellationTokenFactoryMethod(this IBackgroundCommandExecutor command)
        {
            return command.ExecuteCancellationTokenFactoryMethod();
        }

        /// <summary>
        /// Executes the commands ContinuatiionAction.
        /// </summary>
        /// <param name="command">Should be the system under test.</param>
        /// <param name="parameter">Command parameter to be passed to the commands ContinuationAction.</param>
        public static void ExecuteContinuationAction(this IBackgroundCommandExecutor command, object? parameter = null!)
        {
            command.ExecuteContinuationAction(parameter);
        }

        /// <summary>
        /// Executes the commands ErrorHandler.
        /// </summary>
        /// <param name="command">Should be the system under test.</param>
        /// <param name="exception">Exception to be passed to the error callback.</param>
        public static void ExecuteErrorHandler(this IBackgroundCommandExecutor command, Exception exception)
        {
            command.ExecuteErrorHandler(exception);
        }
    }
}