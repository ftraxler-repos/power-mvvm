﻿using System;
using System.Threading.Tasks;
using PowerMVVM.Commands.CommandSettings;
// ReSharper disable MissingXmlDoc - DemoClass

namespace PowerMVVM.Demo.WPF
{
    public class DemoStatusProvider : AStatusProvider
    {
        private readonly MainViewModel _mainViewModel;

        public DemoStatusProvider(MainViewModel mainViewModel)
        {
            _mainViewModel = mainViewModel;
        }

        /// <inheritdoc />
        protected override Task HideLoadingIndicatorInternalAsync()
        {
            _mainViewModel.IsLoading = false;
            return Task.CompletedTask;
        }

        /// <inheritdoc />
        protected override Task ShowLoadingIndicatorInternalAsync()
        {
            _mainViewModel.IsLoading = true;
            return Task.CompletedTask;
        }
    }
}