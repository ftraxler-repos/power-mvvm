﻿using System;
using System.Windows;
using PowerMVVM.Commands;

namespace PowerMVVM.Demo.WPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        public MainWindow()
        {
            InitializeComponent();
            DataContext = new MainViewModel();
        }

        private void CG_Execute(object sender, RoutedEventArgs e)
        {
            GC.Collect(GC.MaxGeneration, GCCollectionMode.Forced, true);
            GC.WaitForPendingFinalizers();
            GC.Collect(GC.MaxGeneration, GCCollectionMode.Forced, true);
        }

        private void CGTest_OnClick(object sender, RoutedEventArgs e)
        {
            Command.StatusProvider = null;
            DataContext = null;
            CG_Execute(null, null);
        }
    }
}