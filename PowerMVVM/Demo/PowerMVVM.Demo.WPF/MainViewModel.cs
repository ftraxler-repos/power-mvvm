﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using PowerMVVM.Commands;

// ReSharper disable MissingXmlDoc - sample project
// ReSharper disable MemberCanBePrivate.Global - ViewModel
namespace PowerMVVM.Demo.WPF
{
    public sealed class MainViewModel : BindableBackend
    {
        private readonly Random _random = new Random();
        private string _backgroundCommandResponseText;
        private CancellationTokenSource _cancellationTokenSource;
        private bool _checkOneChecked;
        private bool _checkTwoChecked;
        private string _commandResponseText;
        private bool _isLoading;

        public MainViewModel()
        {
            // -------------------------------------------------------------------------------------------------
            // PLEASE COMMENT OUT THE FOLLOWING LINE TO TEST DEFAULT ERROR HANDLING BEHAVIOR WHILE IN DEBUG MODE
            Command.LoggingProvider = null!;
            // -------------------------------------------------------------------------------------------------

            Command.StatusProvider = new DemoStatusProvider(this);
        }

        public AutoResetCommand AutoresetCommand { get; } = new AutoResetCommand(() => MessageBox.Show("Auto Reset Command Executed"));

        public string BackgroundCommandResponseText
        {
            get => _backgroundCommandResponseText;
            set
            {
                _backgroundCommandResponseText = AddStringChecksum(value);
                RaisePropertyChanged();
            }
        }

        public ACommand CancelBackgroundCommand => Command.Create(() => _cancellationTokenSource.Cancel());

        public bool CheckOneChecked
        {
            get => _checkOneChecked;
            set
            {
                _checkOneChecked = value;
                RaisePropertyChanged();
            }
        }

        public bool CheckTwoChecked
        {
            get => _checkTwoChecked;
            set
            {
                _checkTwoChecked = value;
                RaisePropertyChanged();
            }
        }

        public string CommandResponseText
        {
            get => _commandResponseText;
            set
            {
                _commandResponseText = AddStringChecksum(value);
                RaisePropertyChanged();
            }
        }

        public ACommand DefaultLoggingProviderAsyncCommand => Command.Create(() => Task.FromException(new NullReferenceException()));

        public ACommand DefaultLoggingProviderCommand => Command.Create(() => throw new NullReferenceException());

        public ACommand HandleErrorCallbackCommand => Command.Create(() => throw new NullReferenceException())
            .SetErrorHandler(ex => CommandResponseText = ex.Message);

        public bool IsLoading
        {
            get => _isLoading;
            set
            {
                _isLoading = value;
                RaisePropertyChanged();
            }
        }

        public BackgroundCommand LongRunningBackgroundCommand => new BackgroundCommand((o, ct) => Task.Delay(12000, ct))
            .ContinueWith(o => BackgroundCommandResponseText = "BackgroundCommand executed successfully")
            .RegisterCancellationTokenFactory(() =>
            {
                _cancellationTokenSource = new CancellationTokenSource();
                return _cancellationTokenSource.Token;
            })
            .SetCancellationHandler(() => MessageBox.Show("BackgroundCommand cancelled"));

        public ACommand MultipleExecutionAsyncCommand => Command.Create(OnMultipleExecutionAsyncCommand)
            .AllowMultipleExecution();

        public ACommand ObservingPropertiesCommand => Command.Create(() => CommandResponseText = "Command executed sucessfully",
                () => CheckOneChecked && CheckTwoChecked)
            .ConfigureRaiseCanExecuteChanged(pco => pco.SetPropertyHolder(this)
                .SubscribeToProperty(nameof(CheckOneChecked))
                .SubscribeToProperty(nameof(CheckTwoChecked)));

        public ACommand SetAutoresetCommand => Command.Create(() => AutoresetCommand.Signal());

        public ACommand SetTextContinuationAsyncCommand => Command.Create(() => Task.Delay(1500))
            .ContinueWith(OnSetTextContinuationAsyncCommand);

        public ACommand SetTextContinuationLoadingAsyncCommand => Command.Create(() => Task.Delay(1500))
            .ContinueWith(OnSetTextContinuationAsyncCommand)
            .UseLoadingIndicator();

        public ACommand SetTextMultipleExecutionAsyncCommand => Command.Create(OnSetTextActionCommand)
            .AllowMultipleExecution();

        private string AddStringChecksum(string value)
        {
            return $"{value} [Checksum:{_random.Next(10000, 99999).ToString()}]";
        }

        private async Task OnMultipleExecutionAsyncCommand()
        {
            await Task.Delay(250);
            MessageBox.Show("Command executed");
        }

        private async Task OnSetTextActionCommand()
        {
            await Task.Delay(1500);
            CommandResponseText = "Updated after 1500ms delay - Waiting another 500ms";
            await Task.Delay(500);
        }

        private void OnSetTextContinuationAsyncCommand()
        {
            CommandResponseText = "Continued command after 1500ms delay";
        }

        public ICommand BindableBackendCommand => Commands.Get(() => MessageBox.Show($"Command Hash is {BindableBackendCommand.GetHashCode()}"), command => command.ContinueWith(() =>MessageBox.Show("continued")));
    }
}