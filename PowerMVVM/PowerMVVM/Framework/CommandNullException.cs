﻿using System;

namespace PowerMVVM.Framework
{
    public class CommandNullException : Exception
    {
        private const string MESSAGE = "Unable to execute the command for this action. The command must not be null!";

        /// <inheritdoc />
        public CommandNullException() : base(MESSAGE) { }
    }
}