﻿using System;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows.Input;
using PowerMVVM.Commands;

namespace PowerMVVM.Framework
{
    public abstract class ACommandHolder
    {
        protected abstract CommandStorage GetFromCommandStorage(object action);

        public ICommand Get(Action action, Func<ACommand, ACommand>? cmd = null, [CallerMemberName] string commandName = null!)
        {
            var storage = GetFromCommandStorageInternal(action);
            if (storage.Command != null)
                return storage.Command;
            var command = Command.Create(action, commandName);
            storage.Command = cmd?.Invoke(command) ?? command;
            return storage.Command;
        }

        public ICommand Get<T>(Action<T> action, Func<ACommand, ACommand>? cmd = null, [CallerMemberName] string commandName = null!)
        {
            var storage = GetFromCommandStorageInternal(action);
            if (storage.Command != null)
                return storage.Command;
            var command = Command.Create(action, commandName);
            storage.Command = cmd?.Invoke(command) ?? command;
            return storage.Command;
        }

        public ICommand Get(Func<Task> action, Func<ACommand, ACommand>? cmd = null, [CallerMemberName] string commandName = null!)
        {
            var storage = GetFromCommandStorageInternal(action);
            if (storage.Command != null)
                return storage.Command;
            var command = Command.Create(action, commandName);
            storage.Command = cmd?.Invoke(command) ?? command;
            return storage.Command;
        }

        public ICommand Get<T>(Func<T, Task> action, Func<ACommand, ACommand>? cmd = null, [CallerMemberName] string commandName = null!)
        {
            var storage = GetFromCommandStorageInternal(action);
            if (storage.Command != null)
                return storage.Command;
            var command = Command.Create(action, commandName);
            storage.Command = cmd?.Invoke(command) ?? command;
            return storage.Command;
        }

        public ICommand Get(Action action,
                            Func<bool> canexecuteChanged,
                            Func<ACommand, ACommand>? config = null,
                            [CallerMemberName] string commandName = null!)
        {
            var storage = GetFromCommandStorageInternal(action);
            if (storage.Command != null)
                return storage.Command;
            var command = Command.Create(action, canexecuteChanged, commandName);
            storage.Command = config?.Invoke(command) ?? command;
            return storage.Command;
        }

        public ICommand Get<T>(Action<T> action,
                               Func<T, bool> canexecuteChanged,
                               Func<ACommand, ACommand>? config = null,
                               [CallerMemberName] string commandName = null!)
        {
            var storage = GetFromCommandStorageInternal(action);
            if (storage.Command != null)
                return storage.Command;
            var command = Command.Create(action, canexecuteChanged, commandName);
            storage.Command = config?.Invoke(command) ?? command;
            return storage.Command;
        }

        public ICommand Get(Func<Task> action,
                            Func<bool> canexecuteChanged,
                            Func<ACommand, ACommand>? config = null,
                            [CallerMemberName] string commandName = null!)
        {
            var storage = GetFromCommandStorageInternal(action);
            if (storage.Command != null)
                return storage.Command;
            var command = Command.Create(action, canexecuteChanged, commandName);
            storage.Command = config?.Invoke(command) ?? command;
            return storage.Command;
        }

        public ICommand Get<T>(Func<T, Task> action,
                               Func<T, bool> canexecuteChanged,
                               Func<ACommand, ACommand>? config = null,
                               [CallerMemberName] string commandName = null!)
        {
            var storage = GetFromCommandStorageInternal(action);
            if (storage.Command != null)
                return storage.Command;
            var command = Command.Create(action, canexecuteChanged, commandName);
            storage.Command = config?.Invoke(command) ?? command;
            return storage.Command;
        }

        public ICommand Get(ICommand command, Func<ACommand, ACommand>? cmd = null, [CallerMemberName] string commandName = null!)
        {
            var storage = GetFromCommandStorageInternal(command);
            if (storage.Command != null)
                return storage.Command;
            var acommand = Command.Create(command, commandName);
            storage.Command = cmd?.Invoke(acommand) ?? acommand;
            return storage.Command;
        }

        private CommandStorage GetFromCommandStorageInternal(object action)
        {
            if (action == null)
                throw new ArgumentNullException(nameof(action));
            var storage = GetFromCommandStorage(action);
            if (storage == null)
                throw new ArgumentNullException(nameof(storage));
            return storage;
        }
    }
}