﻿using System;

namespace PowerMVVM.Framework
{
    public class NullPropertyChangeInterceptor : APropertyChangeInterceptor
    {
        /// <inheritdoc />
        protected override T PropertyIsAboutToChange<T>(string propertyName, T oldValue, T newValue)
        {
            throw new NotImplementedException();
        }

        /// <inheritdoc />
        internal override T Intercept<T>(T oldValue, T newValue, string propertyName)
        {
            return newValue;
        }
    }
}