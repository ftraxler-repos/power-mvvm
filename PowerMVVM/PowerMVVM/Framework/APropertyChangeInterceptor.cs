﻿namespace PowerMVVM.Framework
{
    public abstract class APropertyChangeInterceptor
    {
        protected abstract T PropertyIsAboutToChange<T>(string propertyName, T oldValue, T newValue);

        internal virtual T Intercept<T>(T oldValue, T newValue, string propertyName)
        {
            return PropertyIsAboutToChange(propertyName, oldValue, newValue);
        }
    }
}