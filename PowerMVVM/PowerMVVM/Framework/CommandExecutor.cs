﻿using System.Windows.Input;

namespace PowerMVVM.Framework
{
    internal static class CommandExecutor
    {
        public static void Execute(this ICommand command)
        {
            command.Execute(null!);
        }

        public static void Execute<T>(this ICommand command, T parameter)
        {
            command.Execute(parameter);
        }
    }
}