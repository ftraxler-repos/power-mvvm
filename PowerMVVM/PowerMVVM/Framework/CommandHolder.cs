﻿using System.Collections.Generic;
using PowerMVVM.Commands;

namespace PowerMVVM.Framework
{
    public sealed class CommandHolder : ACommandHolder
    {
        private readonly Dictionary<int, CommandStorage?> _dictionary = new Dictionary<int, CommandStorage?>();

        /// <inheritdoc />
        protected override CommandStorage GetFromCommandStorage(object action)
        {
            var hash = action.GetHashCode();
            if (!_dictionary.ContainsKey(hash))
                _dictionary.Add(hash, new CommandStorage());
            return _dictionary[hash]!; // not null, because initialized when key not existent
        }
    }
}