﻿using PowerMVVM.Commands;

namespace PowerMVVM.Framework
{
    public class CommandStorage
    {
        internal ACommand? Command { get; set; }
    }
}