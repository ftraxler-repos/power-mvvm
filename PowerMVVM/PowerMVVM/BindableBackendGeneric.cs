﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using PowerMVVM.Commands;
using PowerMVVM.Framework;

namespace PowerMVVM
{
    public class BindableBackend<TCommandHolder, TPropertyChangeInterceptor> : BindableBackendBase
        where TCommandHolder : ACommandHolder, new() where TPropertyChangeInterceptor : APropertyChangeInterceptor, new()
    {
        private readonly TCommandHolder _commandHolder;
        private readonly TPropertyChangeInterceptor _interceptor;

        public BindableBackend()
        {
            _commandHolder = new TCommandHolder();
            _interceptor = new TPropertyChangeInterceptor();
        }

        protected ACommandHolder Commands => _commandHolder;

        protected TPropertyChangeInterceptor Interceptor => _interceptor;

        private protected override void SetPropertyInternal<T>(ref T storage, T value, string propertyName)
        {
            value = _interceptor.Intercept(storage, value, propertyName);
            storage = value;
            RaisePropertyChanged(propertyName);
        }

        private protected override bool TrySetPropertyInternal<T>(ref T storage, T value, string propertyName)
        {
            if (EqualityComparer<T>.Default.Equals(storage, value))
                return false;

            value = _interceptor.Intercept(storage, value, propertyName);
            SetProperty(ref storage, value, propertyName);
            return true;
        }

        protected void SetProperty<T>(ref T storage,
                                      T value,
                                      Action onPropertyChanged,
                                      [CallerMemberName] string propertyName = null!)
        {
            SetProperty(ref storage, value, propertyName);
            Commands.Get(onPropertyChanged, cmd => cmd.AllowMultipleExecution(), propertyName).Execute();
        }

        protected void SetProperty<T>(ref T storage,
                                      T value,
                                      Action onPropertyChanged,
                                      Action<Exception> onError,
                                      [CallerMemberName] string propertyName = null!)
        {
            SetProperty(ref storage, value);
            Commands.Get(onPropertyChanged, cmd => cmd.AllowMultipleExecution().SetErrorHandler(onError), propertyName).Execute();
        }

        protected void SetProperty<T>(ref T storage,
                                      T value,
                                      Action<T> onPropertyChanged,
                                      [CallerMemberName] string propertyName = null!)
        {
            SetProperty(ref storage, value, propertyName);
            Commands.Get(onPropertyChanged, cmd => cmd.AllowMultipleExecution(), propertyName).Execute(value);
        }

        protected void SetProperty<T>(ref T storage,
                                      T value,
                                      Action<T> onPropertyChanged,
                                      Action<Exception> onError,
                                      [CallerMemberName] string propertyName = null!)
        {
            SetProperty(ref storage, value);
            Commands.Get(onPropertyChanged, cmd => cmd.AllowMultipleExecution().SetErrorHandler(onError), propertyName).Execute(value);
        }

        protected void SetProperty<T>(ref T storage,
                                      T value,
                                      Func<Task> onPropertyChanged,
                                      [CallerMemberName] string propertyName = null!)
        {
            SetProperty(ref storage, value, propertyName);
            Commands.Get(onPropertyChanged, cmd => cmd.AllowMultipleExecution(), propertyName).Execute();
        }

        protected void SetProperty<T>(ref T storage,
                                      T value,
                                      Func<Task> onPropertyChanged,
                                      Action<Exception> onError,
                                      [CallerMemberName] string propertyName = null!)
        {
            SetProperty(ref storage, value);
            Commands.Get(onPropertyChanged, cmd => cmd.AllowMultipleExecution().SetErrorHandler(onError), propertyName).Execute();
        }

        protected void SetProperty<T>(ref T storage,
                                      T value,
                                      Func<T, Task> onPropertyChanged,
                                      [CallerMemberName] string propertyName = null!)
        {
            SetProperty(ref storage, value, propertyName);
            Commands.Get(onPropertyChanged, cmd => cmd.AllowMultipleExecution(), propertyName).Execute(value);
        }

        protected void SetProperty<T>(ref T storage,
                                      T value,
                                      Func<T, Task> onPropertyChanged,
                                      Action<Exception> onError,
                                      [CallerMemberName] string propertyName = null!)
        {
            SetProperty(ref storage, value);
            Commands.Get(onPropertyChanged, cmd => cmd.AllowMultipleExecution().SetErrorHandler(onError), propertyName).Execute(value);
        }

        protected void SetProperty<T>(ref T storage,
                                      T value,
                                      ACommand onPropertyChangedCommand)
        {
            if (onPropertyChangedCommand == null)
                throw new CommandNullException();

            SetProperty(ref storage, value);
            onPropertyChangedCommand.Execute(value);
        }

        protected void TrySetProperty<T>(ref T storage,
                                         T value,
                                         Action onPropertyChanged,
                                         [CallerMemberName] string propertyName = null!)
        {
            if (TrySetProperty(ref storage, value, propertyName))
                Commands.Get(onPropertyChanged, cmd => cmd.AllowMultipleExecution(), propertyName).Execute();
        }

        protected void TrySetProperty<T>(ref T storage,
                                         T value,
                                         Action onPropertyChanged,
                                         Action<Exception> onError,
                                         [CallerMemberName] string propertyName = null!)
        {
            if (TrySetProperty(ref storage, value))
                Commands.Get(onPropertyChanged, cmd => cmd.AllowMultipleExecution().SetErrorHandler(onError), propertyName).Execute();
        }

        protected void TrySetProperty<T>(ref T storage,
                                         T value,
                                         Action<T> onPropertyChanged,
                                         [CallerMemberName] string propertyName = null!)
        {
            if (TrySetProperty(ref storage, value, propertyName))
                Commands.Get(onPropertyChanged, cmd => cmd.AllowMultipleExecution(), propertyName).Execute(value);
        }

        protected void TrySetProperty<T>(ref T storage,
                                         T value,
                                         Action<T> onPropertyChanged,
                                         Action<Exception> onError,
                                         [CallerMemberName] string propertyName = null!)
        {
            if (TrySetProperty(ref storage, value))
                Commands.Get(onPropertyChanged, cmd => cmd.AllowMultipleExecution().SetErrorHandler(onError), propertyName).Execute(value);
        }

        protected void TrySetProperty<T>(ref T storage,
                                         T value,
                                         Func<Task> onPropertyChanged,
                                         [CallerMemberName] string propertyName = null!)
        {
            if (TrySetProperty(ref storage, value, propertyName))
                Commands.Get(onPropertyChanged, cmd => cmd.AllowMultipleExecution(), propertyName).Execute();
        }

        protected void TrySetProperty<T>(ref T storage,
                                         T value,
                                         Func<Task> onPropertyChanged,
                                         Action<Exception> onError,
                                         [CallerMemberName] string propertyName = null!)
        {
            if (TrySetProperty(ref storage, value))
                Commands.Get(onPropertyChanged, cmd => cmd.AllowMultipleExecution().SetErrorHandler(onError), propertyName).Execute();
        }

        protected void TrySetProperty<T>(ref T storage,
                                         T value,
                                         Func<T, Task> onPropertyChanged,
                                         [CallerMemberName] string propertyName = null!)
        {
            if (TrySetProperty(ref storage, value, propertyName))
                Commands.Get(onPropertyChanged, cmd => cmd.AllowMultipleExecution(), propertyName).Execute(value);
        }

        protected void TrySetProperty<T>(ref T storage,
                                         T value,
                                         Func<T, Task> onPropertyChanged,
                                         Action<Exception> onError,
                                         [CallerMemberName] string propertyName = null!)
        {
            if (TrySetProperty(ref storage, value))
                Commands.Get(onPropertyChanged, cmd => cmd.AllowMultipleExecution().SetErrorHandler(onError), propertyName).Execute(value);
        }

        protected void TrySetProperty<T>(ref T storage,
                                         T value,
                                         ACommand onPropertyChangedCommand)
        {
            if (onPropertyChangedCommand == null)
                throw new CommandNullException();

            if (TrySetProperty(ref storage, value))
                onPropertyChangedCommand.Execute(value);
        }
    }
}