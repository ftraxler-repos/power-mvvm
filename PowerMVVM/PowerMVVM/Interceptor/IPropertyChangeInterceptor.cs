﻿namespace PowerMVVM.Interceptor
{
    public interface IPropertyChangeInterceptor
    {
        public IPropertyChangeInterceptorRule<T> ForProperty<T>(string name);
    }
}