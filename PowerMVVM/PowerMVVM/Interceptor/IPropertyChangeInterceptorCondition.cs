﻿using System;

namespace PowerMVVM.Interceptor
{
    public interface IPropertyChangeInterceptorCondition<T>
    {
        public void Do(Func<T, T, T> setValue);
    }
}