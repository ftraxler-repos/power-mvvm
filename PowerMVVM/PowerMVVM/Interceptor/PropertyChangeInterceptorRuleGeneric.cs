﻿using System;

namespace PowerMVVM.Interceptor
{
    internal class PropertyChangeInterceptorRule<T> : PropertyChangeInterceptorRuleBase<T>, IPropertyChangeInterceptorRule<T>
    {
        private Func<T, T, T>? _action;
        private Func<T, T, bool>? _condition;

        /// <inheritdoc />
        public void Do(Func<T, T, T> setValue)
        {
            _action = setValue;
        }

        /// <inheritdoc />
        public IPropertyChangeInterceptorCondition<T> When(Func<T, T, bool> condition)
        {
            _condition = condition;
            return this;
        }

        /// <inheritdoc />
        protected override T ExecuteInternal(T oldValue, T newValue)
        {
            if (_action == null)
                throw new ArgumentNullException(nameof(_action), "An interceptor rule needs to define an action");

            if (_condition == null)
                return _action.Invoke(oldValue, newValue);

            var canExecute = _condition.Invoke(oldValue, newValue);
            if (canExecute)
                return _action.Invoke(oldValue, newValue);
            return oldValue;
        }
    }
}