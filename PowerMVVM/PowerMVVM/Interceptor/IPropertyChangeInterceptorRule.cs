﻿using System;

namespace PowerMVVM.Interceptor
{
    public interface IPropertyChangeInterceptorRule<T> : IPropertyChangeInterceptorCondition<T>
    {
        public IPropertyChangeInterceptorCondition<T> When(Func<T, T, bool> condition);
    }
}