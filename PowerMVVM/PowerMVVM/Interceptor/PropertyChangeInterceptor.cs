﻿using System;
using System.Collections;
using PowerMVVM.Framework;

namespace PowerMVVM.Interceptor
{
    public class PropertyChangeInterceptor : APropertyChangeInterceptor, IPropertyChangeInterceptor
    {
        private readonly Hashtable _rules = new Hashtable();

        /// <inheritdoc />
        IPropertyChangeInterceptorRule<T> IPropertyChangeInterceptor.ForProperty<T>(string name)
        {
            var rule = new PropertyChangeInterceptorRule<T>();
            if (_rules.ContainsKey(name))
                throw new InvalidOperationException($"An interceptor rule for the property named '{name}' is already defined");

            _rules.Add(name, rule);
            return rule;
        }

        /// <inheritdoc />
        protected override T PropertyIsAboutToChange<T>(string propertyName, T oldValue, T newValue)
        {
            if (_rules.ContainsKey(propertyName))
            {
                if (!(_rules[propertyName] is PropertyChangeInterceptorRuleBase<T> rule))
                    throw new InvalidCastException(
                        $"The interceptor rule for the property named '{propertyName}' defines another type than the property. The rule should be for the type '{typeof(T)}'.");
                return rule.Execute(oldValue, newValue);
            }
            return newValue;
        }

        public void Configure(Action<IPropertyChangeInterceptor> interceptor)
        {
            interceptor.Invoke(this);
        }
    }
}