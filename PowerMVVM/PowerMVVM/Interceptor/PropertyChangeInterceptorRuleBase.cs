﻿namespace PowerMVVM.Interceptor
{
    internal abstract class PropertyChangeInterceptorRuleBase<T>
    {
        protected abstract T ExecuteInternal(T oldValue, T newValue);

        public T Execute(T oldValue, T newValue)
        {
            return ExecuteInternal(oldValue, newValue);
        }
    }
}