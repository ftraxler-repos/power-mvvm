﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace PowerMVVM
{
    public class BindableBackendBase : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler? PropertyChanged;

        protected virtual void OnPropertyChanged(PropertyChangedEventArgs e) { }

        protected void RaisePropertyChanged([CallerMemberName] string propertyName = null!)
        {
            var eventArgs = new PropertyChangedEventArgs(propertyName);
            PropertyChanged?.Invoke(this, eventArgs);
            OnPropertyChanged(eventArgs);
        }

        protected void SetProperty<T>(ref T storage, T value, [CallerMemberName] string propertyName = null!)
        {
            SetPropertyInternal(ref storage, value, propertyName);
        }

        protected bool TrySetProperty<T>(ref T storage, T value, [CallerMemberName] string propertyName = null!)
        {
            return TrySetPropertyInternal(ref storage, value, propertyName);
        }

        private protected virtual void SetPropertyInternal<T>(ref T storage, T value, string propertyName)
        {
            storage = value;
            RaisePropertyChanged(propertyName);
        }

        private protected virtual bool TrySetPropertyInternal<T>(ref T storage, T value, string propertyName)
        {
            if (EqualityComparer<T>.Default.Equals(storage, value))
                return false;

            SetProperty(ref storage, value, propertyName);
            return true;
        }
    }
}