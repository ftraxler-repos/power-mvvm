﻿using PowerMVVM.Framework;

namespace PowerMVVM
{
    public class BindableBackend : BindableBackend<CommandHolder, NullPropertyChangeInterceptor> { }

    public class BindableBackend<TPropertyChangeObserver> : BindableBackend<CommandHolder, TPropertyChangeObserver>
        where TPropertyChangeObserver : APropertyChangeInterceptor, new() { }
}