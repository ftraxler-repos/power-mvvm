﻿using System;
using System.Windows.Input;

namespace PowerMVVM.Tests.Benchmark
{
    public class SimpleCommand : ICommand
    {
        /// <inheritdoc />
        public event EventHandler CanExecuteChanged;

        private readonly Action _action;
        private readonly Func<bool> _canExecute;

        public SimpleCommand(Action action, Func<bool> canExecute)
        {
            _action = action;
            _canExecute = canExecute;
        }

        /// <inheritdoc />
        public bool CanExecute(object parameter)
        {
            return _canExecute();
        }

        /// <inheritdoc />
        public void Execute(object parameter)
        {
            _action();
        }

        public void RaiseCanExecutechanged()
        {
            CanExecuteChanged?.Invoke(this, EventArgs.Empty);
        }
    }
}