﻿using System.Windows.Input;
using BenchmarkDotNet.Attributes;
using PowerMVVM.Commands;

namespace PowerMVVM.Tests.Benchmark.Commands
{
    public class ACommandSync : CommandBenchmarkBase
    {
        private ACommand _acommand;
        private ICommand _command;

        [GlobalSetup(Targets = new[]
        {
            nameof(Execute),
            nameof(CanExecute),
        })]
        public void _SetUp()
        {
            _command = _acommand = Command.Create(Action);
        }

        [IterationSetup(Target = nameof(SubscribeToPropertyChanged))]
        public void _SetUp_Everytime()
        {
            _command = _acommand = Command.Create(Action);
        }

        [IterationSetup(Target = nameof(RaisePropertyChanged))]
        public void _SetUp_RaisePropertyChanged()
        {
            _acommand = Command.Create(Action)
                .ConfigureRaiseCanExecuteChanged(pco => pco.SetPropertyHolder(this).SubscribeToProperty(nameof(PARAMETER)));
        }

        [Benchmark]
        [BenchmarkCategory(nameof(CanExecute))]
        public void CanExecute()
        {
            _command.CanExecute(PARAMETER);
        }

        [Benchmark]
        [BenchmarkCategory(nameof(Creation))]
        public void Creation()
        {
            _command = Command.Create(Action);
        }

        [Benchmark]
        [BenchmarkCategory(nameof(Execute))]
        public void Execute()
        {
            _command.Execute(PARAMETER);
        }

        [Benchmark(Baseline = true)]
        [BenchmarkCategory(nameof(RaisePropertyChanged))]
        public void RaisePropertyChanged()
        {
            OnPropertyChanged(nameof(PARAMETER));
        }

        [Benchmark(Baseline = true)]
        [BenchmarkCategory(nameof(SubscribeToPropertyChanged))]
        public void SubscribeToPropertyChanged()
        {
            _acommand.ConfigureRaiseCanExecuteChanged(pco => pco.SetPropertyHolder(this).SubscribeToProperty(nameof(PARAMETER)));
        }
    }
}