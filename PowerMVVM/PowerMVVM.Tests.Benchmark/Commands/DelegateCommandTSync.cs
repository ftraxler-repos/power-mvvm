﻿using System.Windows.Input;
using BenchmarkDotNet.Attributes;
using PowerMVVM.Commands;
using Prism.Commands;

namespace PowerMVVM.Tests.Benchmark.Commands
{
    public class DelegateCommandTSync : CommandBenchmarkBase
    {
        private DelegateCommand<object> _delegateCommand;
        private ICommand _command;

        [GlobalSetup(Targets = new[]
        {
            nameof(Execute),
            nameof(CanExecute)
        })]
        public void _SetUp()
        {
            _command = _delegateCommand = new DelegateCommand<object>(Function);
        }

        [IterationSetup(Target = nameof(SubscribeToPropertyChanged))]
        public void _SetUp_Everytime()
        {
            _command = _delegateCommand = new DelegateCommand<object>(Function);
        }

        [IterationSetup(Target = nameof(RaisePropertyChanged))]
        public void _SetUp_RaisePropertyChanged()
        {
            _delegateCommand = new DelegateCommand<object>(Function).ObservesProperty(() => Action);
        }

        [Benchmark]
        [BenchmarkCategory(nameof(CanExecute))]
        public void CanExecute()
        {
            _command.CanExecute(PARAMETER);
        }

        [Benchmark]
        [BenchmarkCategory(nameof(Creation))]
        public void Creation()
        {
            _command = Command.Create(AsyncAction);
        }

        [Benchmark]
        [BenchmarkCategory(nameof(Execute))]
        public void Execute()
        {
            _command.Execute(PARAMETER);
        }

        [Benchmark]
        [BenchmarkCategory(nameof(RaisePropertyChanged))]
        public void RaisePropertyChanged()
        {
            OnPropertyChanged(nameof(Action));
        }

        [Benchmark]
        [BenchmarkCategory(nameof(SubscribeToPropertyChanged))]
        public void SubscribeToPropertyChanged()
        {
            _delegateCommand.ObservesProperty(() => Action);
        }
    }
}