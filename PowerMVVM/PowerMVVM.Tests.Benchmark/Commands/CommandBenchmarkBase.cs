﻿using System;
using System.ComponentModel;
using System.Threading.Tasks;

namespace PowerMVVM.Tests.Benchmark.Commands
{
    public class CommandBenchmarkBase : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected const int PARAMETER = 1234567890;

        protected Action Action => () => { CalculateNumbersSum(); };

        protected Func<Task> AsyncAction => () => { return Task.Run(() => { CalculateNumbersSum(); }); };

        protected Action<object> Function => value =>
        {
            value.GetHashCode();
            CalculateNumbersSum();
        };

        protected Func<object, Task> AsyncFunction => value =>
        {
            return Task.Run(() =>
            {
                value.GetHashCode();
                CalculateNumbersSum();
            });
        };

        protected void OnPropertyChanged(string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private long CalculateNumbersSum()
        {
            var i = 1_234_098_456_789_150_246;
            long result = 0;
            while (i > 0)
            {
                result += i % 10;
                i /= 10;
            }
            return result;
        }
    }
}