﻿using System.Windows.Input;
using BenchmarkDotNet.Attributes;

namespace PowerMVVM.Tests.Benchmark.Commands
{
    public class SimpleCommandAsyncVoid : CommandBenchmarkBase
    {
        private ICommand _command;

        [GlobalSetup(Targets = new[]
        {
            nameof(Execute),
            nameof(CanExecute)
        })]
        public void _SetUp()
        {
            _command = new SimpleCommand(async () => await AsyncAction(), () => false);
        }

        [Benchmark]
        [BenchmarkCategory(nameof(CanExecute))]
        public void CanExecute()
        {
            _command.CanExecute(PARAMETER);
        }

        [Benchmark]
        [BenchmarkCategory(nameof(Creation))]
        public void Creation()
        {
            _command = new SimpleCommand(async () => await AsyncAction(), () => false);
        }

        [Benchmark]
        [BenchmarkCategory(nameof(Execute))]
        public void Execute()
        {
            _command.Execute(PARAMETER);
        }
    }
}