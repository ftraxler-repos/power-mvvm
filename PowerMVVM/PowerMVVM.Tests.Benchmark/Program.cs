﻿using System.Linq;
using BenchmarkDotNet.Columns;
using BenchmarkDotNet.Configs;
using BenchmarkDotNet.Diagnosers;
using BenchmarkDotNet.Exporters;
using BenchmarkDotNet.Jobs;
using BenchmarkDotNet.Running;
using PowerMVVM.Tests.Benchmark.Commands;

namespace PowerMVVM.Tests.Benchmark
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var config = ManualConfig.Create(DefaultConfig.Instance)
                .WithOptions(ConfigOptions.JoinSummary)
                .AddDiagnoser(MemoryDiagnoser.Default)
                .AddColumn(CategoriesColumn.Default)
                .AddLogicalGroupRules(BenchmarkLogicalGroupRule.ByCategory);

            if (args.Contains("-ci"))
            {
                config.AddJob(new Job().WithLaunchCount(2).WithWarmupCount(10).WithIterationCount(25));
            }
            else
            {
                config.AddJob(new Job().WithLaunchCount(1).WithWarmupCount(3).WithIterationCount(10));
            }

            BenchmarkRunner.Run(new[]
            {
                BenchmarkConverter.TypeToBenchmarks(typeof(SimpleCommandSync), config),
                BenchmarkConverter.TypeToBenchmarks(typeof(SimpleCommandAsyncVoid), config),
                BenchmarkConverter.TypeToBenchmarks(typeof(ACommandSync), config),
                BenchmarkConverter.TypeToBenchmarks(typeof(ACommandAsync), config),
                BenchmarkConverter.TypeToBenchmarks(typeof(ACommandTSync), config),
                BenchmarkConverter.TypeToBenchmarks(typeof(ACommandTAsync), config),
                BenchmarkConverter.TypeToBenchmarks(typeof(DelegateCommandSync), config),
                BenchmarkConverter.TypeToBenchmarks(typeof(DelegateCommandAsyncVoid), config),
                BenchmarkConverter.TypeToBenchmarks(typeof(DelegateCommandTSync), config),
                BenchmarkConverter.TypeToBenchmarks(typeof(DelegateCommandTAsyncVoid), config),
            });

        }
    }
}