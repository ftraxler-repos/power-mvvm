﻿using System;
using FluentAssertions;
using NUnit.Framework;
using PowerMVVM.Interceptor;

namespace PowerMVVM.Tests.Main
{
    [TestFixture]
    public class PropertyChangeInterceptorRuleTest
    {
        private PropertyChangeInterceptorRule<int> _systemUnderTest;

        [SetUp]
        public void Setup()
        {
            _systemUnderTest = new PropertyChangeInterceptorRule<int>();
        }

        [Test]
        public void Execute_DoesNotReturnDoValue_WhenWhenConditionValidatesFalse()
        {
            _systemUnderTest.When((o, n) => o != n).Do((o, n) => 2);

            var result = _systemUnderTest.Execute(0, 0);

            result.Should().NotBe(2);
        }

        [Test]
        public void Execute_ReturnsDoValue_WhenNoWhenConditionConfigured()
        {
            _systemUnderTest.Do((o, n) => 1);

            var result = _systemUnderTest.Execute(0, 0);

            result.Should().Be(1);
        }

        [Test]
        public void Execute_ReturnsDoValue_WhenWhenConditionValidatesTrue()
        {
            _systemUnderTest.When((o, n) => o != n).Do((o, n) => 2);

            var result = _systemUnderTest.Execute(0, 1);

            result.Should().Be(2);
        }

        [Test]
        public void Execute_RunsDoAction()
        {
            var actionCalled = false;
            _systemUnderTest.Do((o, n) =>
            {
                actionCalled = true;
                return 1;
            });

            _systemUnderTest.Execute(0, 0);

            actionCalled.Should().BeTrue();
        }

        [Test]
        public void Execute_Throws_WhenNoDoConfigured()
        {
            _systemUnderTest.Invoking(sut => sut.Execute(0, 0))
                .Should()
                .Throw<ArgumentNullException>();
        }
    }
}