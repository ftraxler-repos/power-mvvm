﻿using System;
using FluentAssertions;
using NUnit.Framework;
using PowerMVVM.Interceptor;

namespace PowerMVVM.Tests.Main
{
    [TestFixture]
    public class PropertyChangeInterceptorTest
    {
        private PropertyChangeInterceptor _systemUnderTest;
        private const string testPropertName = "interceptor_test_propertyame";

        [SetUp]
        public void Setup()
        {
            _systemUnderTest = new PropertyChangeInterceptor();
        }

        [Test]
        public void Configure_AddsAnInterceptorRule_Intercepts()
        {
            _systemUnderTest.Configure(interceptor => interceptor.ForProperty<int>(testPropertName).Do((o, n) => 2));
            var result = _systemUnderTest.Intercept(0, 1, testPropertName);
            result.Should().Be(2);
        }

        [Test]
        public void Configure_AddsAnInterceptorRule_ThrowsWhenAlreadySetForProperty()
        {
            _systemUnderTest.Configure(interceptor => interceptor.ForProperty<int>(testPropertName).Do((o, n) => n));
            _systemUnderTest.Invoking(sut
                    => sut.Configure(interceptor => interceptor.ForProperty<int>(testPropertName).When((o, n) => o != n).Do((o, n) => n)))
                .Should()
                .Throw<InvalidOperationException>();
        }

        [Test]
        public void ConfiguringOtherType_ThanInterceptInvokesOn_Throws()
        {
            _systemUnderTest.Configure(interceptor => interceptor.ForProperty<bool>(testPropertName).Do((o, n) => n));
            _systemUnderTest.Invoking(sut => sut.Intercept(0, 1, testPropertName)).Should().Throw<InvalidCastException>();
        }

        [Test]
        public void DoesNotThrow_WhenNoRulerCreatedForProperty()
        {
            _systemUnderTest.Invoking(sut => sut.Intercept(0, 1, testPropertName)).Should().NotThrow();
        }

        [Test]
        public void ReturnsNewValue_WhenNoRulerCreatedForProperty()
        {
            var result = _systemUnderTest.Intercept(0, 1, testPropertName);
            result.Should().Be(1);
        }
    }
}