﻿namespace PowerMVVM.Tests.Main
{
    public class MockBindableBackendBase : BindableBackendBase
    {
        public void DoRaisePropertyChanged(string propertyName)
        {
            RaisePropertyChanged(propertyName);
        }

        public void DoSetProperty<T>(ref T storage, T value, string propertyName)
        {
            SetProperty(ref storage, value, propertyName);
        }

        public bool DoTrySetProperty<T>(ref T storage, T value, string propertyName)
        {
            return TrySetProperty(ref storage, value, propertyName);
        }
    }
}