﻿using System;
using System.ComponentModel;

namespace PowerMVVM.Tests.Main.TestHelper
{
    public class PropertyChangedEvent
    {
        private string _properyName;
        private object _sender;

        public PropertyChangedEvent() { }

        public PropertyChangedEvent(object sender, PropertyChangedEventArgs args)
        {
            SetValues(sender, args);
        }

        public bool HasProperty => !string.IsNullOrEmpty(_properyName);

        public bool WasCalled { get; private set; }

        public bool HasSender => _sender != null;

        public bool IsProperty(string propertyName)
        {
            return _properyName == propertyName;
        }

        public bool IsSender(object expected)
        {
            return _sender == expected;
        }

        public void Receiver(object sender, PropertyChangedEventArgs e)
        {
            SetValues(sender, e);
        }

        public void Reset()
        {
            WasCalled = false;
            _sender = null;
            _properyName = null;
        }

        private void SetValues(object sender, PropertyChangedEventArgs args)
        {
            if (args == null)
                throw new ArgumentNullException(nameof(args));

            WasCalled = true;
            _sender = sender;
            _properyName = args.PropertyName;
        }
    }
}