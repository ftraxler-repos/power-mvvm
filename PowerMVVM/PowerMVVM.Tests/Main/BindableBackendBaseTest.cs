﻿using FluentAssertions;
using NUnit.Framework;
using PowerMVVM.Tests.Main.TestHelper;

namespace PowerMVVM.Tests.Main
{
    [TestFixture]
    public class BindableBackendBaseTest
    {
        private MockBindableBackendBase _systemUnderTest;
        private PropertyChangedEvent _event;
        private const string testPropertyName = "unit_test_property";

        [SetUp]
        public void Setup()
        {
            _event = new PropertyChangedEvent();
            _systemUnderTest = new MockBindableBackendBase();
            _systemUnderTest.PropertyChanged += _event.Receiver;
        }

        [Test]
        public void RaisePropertyChanged_CallsEvent()
        {
            _systemUnderTest.DoRaisePropertyChanged(testPropertyName);
            _event.WasCalled.Should().BeTrue();
        }

        [Test]
        public void RaisePropertyChanged_CallsEvent_CorrectPropertyName()
        {
            _systemUnderTest.DoRaisePropertyChanged(testPropertyName);
            _event.IsProperty(testPropertyName).Should().BeTrue();
        }

        [Test]
        public void RaisePropertyChanged_CallsEvent_WithCorrectSender()
        {
            _systemUnderTest.DoRaisePropertyChanged(testPropertyName);
            _event.IsSender(_systemUnderTest).Should().BeTrue();
        }

        [Test]
        public void SetProperty_CallsEvent()
        {
            var storage = 0;
            _systemUnderTest.DoSetProperty(ref storage, 1, testPropertyName);

            _event.WasCalled.Should().BeTrue();
        }

        [Test]
        public void SetProperty_CallsEvent_WithCorrectPropertyName()
        {
            var storage = 0;
            _systemUnderTest.DoSetProperty(ref storage, 1, testPropertyName);

            _event.IsProperty(testPropertyName).Should().BeTrue();
        }

        [Test]
        public void SetProperty_CallsEvent_WithCorrectSender()
        {
            var storage = 0;
            _systemUnderTest.DoSetProperty(ref storage, 1, testPropertyName);

            _event.IsSender(_systemUnderTest).Should().BeTrue();
        }

        [Test]
        public void SetProperty_SetsStorageToValue()
        {
            var storage = 0;
            _systemUnderTest.DoSetProperty(ref storage, 1, testPropertyName);

            storage.Should().Be(1);
        }

        [Test]
        public void TrySetProperty_CallsEvent()
        {
            var storage = 0;
            _systemUnderTest.DoTrySetProperty(ref storage, 1, testPropertyName);

            _event.WasCalled.Should().BeTrue();
        }

        [Test]
        public void TrySetProperty_CallsEvent_WithCorrectPropertyName()
        {
            var storage = 0;
            _systemUnderTest.DoTrySetProperty(ref storage, 1, testPropertyName);

            _event.IsProperty(testPropertyName).Should().BeTrue();
        }

        [Test]
        public void TrySetProperty_CallsEvent_WithCorrectSender()
        {
            var storage = 0;
            _systemUnderTest.DoTrySetProperty(ref storage, 1, testPropertyName);

            _event.IsSender(_systemUnderTest).Should().BeTrue();
        }

        [Test]
        public void TrySetProperty_DoesNotRaiseProperty_WhenStorageAlreadyContainsValue_Bool()
        {
            var storage = true;
            _systemUnderTest.DoTrySetProperty(ref storage, true, testPropertyName);

            _event.WasCalled.Should().BeFalse();
        }

        [Test]
        public void TrySetProperty_DoesNotRaiseProperty_WhenStorageAlreadyContainsValue_Int()
        {
            var storage = 7;
            _systemUnderTest.DoTrySetProperty(ref storage, 7, testPropertyName);

            _event.WasCalled.Should().BeFalse();
        }

        [Test]
        public void TrySetProperty_DoesNotRaiseProperty_WhenStorageAlreadyContainsValue_Object()
        {
            var obj1 = new object();
            var storage = obj1;
            var couldSetValue = _systemUnderTest.DoTrySetProperty(ref storage, obj1, testPropertyName);

            _event.WasCalled.Should().BeFalse();
        }

        [Test]
        public void TrySetProperty_ReturnsFalse_WhenStorageAlreadyContainsValue_Bool()
        {
            var storage = true;
            var couldSetValue = _systemUnderTest.DoTrySetProperty(ref storage, true, testPropertyName);

            couldSetValue.Should().BeFalse();
        }

        [Test]
        public void TrySetProperty_ReturnsFalse_WhenStorageAlreadyContainsValue_Int()
        {
            var storage = 7;
            var couldSetValue = _systemUnderTest.DoTrySetProperty(ref storage, 7, testPropertyName);

            couldSetValue.Should().BeFalse();
        }

        [Test]
        public void TrySetProperty_ReturnsFalse_WhenStorageAlreadyContainsValue_Object()
        {
            var obj1 = new object();
            var storage = obj1;
            var couldSetValue = _systemUnderTest.DoTrySetProperty(ref storage, obj1, testPropertyName);

            couldSetValue.Should().BeFalse();
        }

        [Test]
        public void TrySetProperty_ReturnsTrue_WhenStorageDoesNotContainValue_Bool()
        {
            var storage = true;
            var couldSetValue = _systemUnderTest.DoTrySetProperty(ref storage, false, testPropertyName);

            couldSetValue.Should().BeTrue();
        }

        [Test]
        public void TrySetProperty_ReturnsTrue_WhenStorageDoesNotContainValue_Int()
        {
            var storage = 0;
            var couldSetValue = _systemUnderTest.DoTrySetProperty(ref storage, 1, testPropertyName);

            couldSetValue.Should().BeTrue();
        }

        [Test]
        public void TrySetProperty_ReturnsTrue_WhenStorageDoesNotContainValue_Object()
        {
            var obj1 = new object();
            var obj2 = new object();
            var storage = obj1;
            var couldSetValue = _systemUnderTest.DoTrySetProperty(ref storage, obj2, testPropertyName);

            couldSetValue.Should().BeTrue();
        }

        [Test]
        public void TrySetProperty_SetsStorageToValue_Bool()
        {
            var storage = true;
            _systemUnderTest.DoTrySetProperty(ref storage, false, testPropertyName);

            storage.Should().Be(false);
        }

        [Test]
        public void TrySetProperty_SetsStorageToValue_Int()
        {
            var storage = 0;
            _systemUnderTest.DoTrySetProperty(ref storage, 1, testPropertyName);

            storage.Should().Be(1);
        }

        [Test]
        public void TrySetProperty_SetsStorageToValue_Object()
        {
            var obj1 = new object();
            var obj2 = new object();
            var storage = obj1;
            _systemUnderTest.DoTrySetProperty(ref storage, obj2, testPropertyName);

            storage.Should().Be(obj2);
        }
    }
}