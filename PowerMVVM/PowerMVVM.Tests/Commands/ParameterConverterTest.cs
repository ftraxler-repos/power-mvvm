﻿using NSubstitute;
using NUnit.Framework;
using PowerMVVM.Commands;
using PowerMVVM.Commands.CommandSettings;
using PowerMVVM.Tests.Commands.Mocks;
using PowerMVVM.Tests.Commands.TestHelper;

// ReSharper disable ExpressionIsAlwaysNull - Unit Tests
namespace PowerMVVM.Tests.Commands
{
    [TestFixture]
    [UsesCommandSettings]
    public class ParameterConverterTest
    {
        private ILoggingProvider _mockLoggingProvider;

        [SetUp]
        public void Setup()
        {
            _mockLoggingProvider = Substitute.For<ILoggingProvider>();
            Command.LoggingProvider = _mockLoggingProvider;
        }

        [Test]
        public void Converts_NullToCorrectType()
        {
            DummyClass obj = null;
            // ReSharper disable once SuggestVarOrType_SimpleTypes - Show that return type is desired type but null
            DummyClass result = obj.To<DummyClass>(null);
            Assert.That(result, Is.Null);
        }

        [Test]
        public void Converts_ToBaseType()
        {
            object obj = new DummyInherited();
            // ReSharper disable once SuggestVarOrType_SimpleTypes - Show that return type is desired type
            DummyBase result = obj.To<DummyBase>(null);
            Assert.That(result, Is.TypeOf<DummyInherited>());
        }

        [Test]
        public void Converts_ToCorrectType()
        {
            object obj = new DummyClass();
            var result = obj.To<DummyClass>(null);
            Assert.That(result, Is.TypeOf<DummyClass>());
        }

        [Test]
        public void Converts_ToString_Null()
        {
            object obj = null;
            // ReSharper disable once SuggestVarOrType_SimpleTypes - Show that return type is desired type
            var result = obj.To<string>(null);
            Assert.That(result, Is.Null);
        }

        [Test]
        public void Converts_ToString_StringEmpty()
        {
            var obj = string.Empty;
            var result = obj.To<string>(null);
            Assert.That(result, Is.Empty);
        }

        [Test]
        public void Throws_WhenNullNotAllowed_Int()
        {
            DummyClass obj = null;
            obj.To<int>(null);
            _mockLoggingProvider.Received().OnException(Arg.Any<CommandParameterNullException>());
        }

        [Test]
        public void Throws_WhenNullNotAllowed_Struct()
        {
            DummyClass obj = null;
            obj.To<DummyStruct>(null);
            _mockLoggingProvider.Received().OnException(Arg.Any<CommandParameterNullException>());
        }

        [Test]
        public void Throws_WhenTypesDoNotMatch()
        {
            object obj = new DummyClass();
            obj.To<DummyBase>(null);
            _mockLoggingProvider.Received().OnException(Arg.Any<CommandParameterCastException>());
        }
    }
}