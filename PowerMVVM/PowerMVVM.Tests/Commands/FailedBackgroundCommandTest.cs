﻿using System;
using System.Threading;
using System.Threading.Tasks;
using FluentAssertions;
using NSubstitute;
using NUnit.Framework;
using PowerMVVM.Commands;
using PowerMVVM.Commands.CommandSettings;
using PowerMVVM.Tests.Commands.TestHelper;

namespace PowerMVVM.Tests.Commands
{
    [TestFixture]
    [UsesCommandSettings]
    [NonParallelizable]
    public class FailedBackgroundCommandTest
    {
        private ILoggingProvider _mockLoggigngProvider;
        private BackgroundCommand _systemUnderTest;
        private AStatusProvider _mockStatusProvider;

        [SetUp]
        public void SetUp()
        {
            _mockLoggigngProvider = Substitute.For<ILoggingProvider>();
            Command.LoggingProvider = _mockLoggigngProvider;
            _mockStatusProvider = Substitute.For<AStatusProvider>();
            Command.StatusProvider = _mockStatusProvider;

            Task MockAction(object o, CancellationToken ct)
            {
                return Task.FromException(new NullReferenceException());
            }

            bool MockCanExecute(object o)
            {
                return true;
            }

            _systemUnderTest = new BackgroundCommand(MockAction, MockCanExecute);
        }

        [Test]
        public async Task ExecuteAsync_CallsLoadingIndicator_WithArgumentFalse()
        {
            await _systemUnderTest.ExecuteAsync();

            await _mockStatusProvider.Received().ShowLoadingIndicatorAsync(false);
        }

        [Test]
        public async Task ExecuteAsync_DoesNotCallContinuationAction()
        {
            var continuationActionCalled = false;
            _systemUnderTest.ContinueWith(o => continuationActionCalled = true);
            await _systemUnderTest.ExecuteAsync();

            continuationActionCalled.Should().BeFalse();
        }

        [Test]
        public async Task ExecuteAsync_DoesNotUseAnyLoadingIndicator_WhenConfigured()
        {
            await _systemUnderTest.ExecuteAsync();

            await _mockStatusProvider.DidNotReceive().ShowLoadingIndicatorAsync(true);
            await _mockStatusProvider.DidNotReceive().ShowLoadingIndicatorAsync(false);
        }

        [Test]
        public async Task ExecuteAsync_DoesNotUseAnyLoadingIndicator_WhenFinished()
        {
            await _systemUnderTest.ExecuteAsync();

            await _mockStatusProvider.DidNotReceive().HideLoadingIndicatorAsync(true);
            await _mockStatusProvider.DidNotReceive().HideLoadingIndicatorAsync(false);
        }

        [Test]
        public async Task ExecuteAsync_InvokesErrorHandler()
        {
            var errorHandlerCalled = false;
            _systemUnderTest.SetErrorHandler(ex => errorHandlerCalled = true);
            await _systemUnderTest.ExecuteAsync();

            errorHandlerCalled.Should().BeTrue();
        }

        [Test]
        public async Task ExecuteAsync_Logs_OnException()
        {
            await _systemUnderTest.ExecuteAsync();

            _mockLoggigngProvider.Received().OnException(Arg.Any<CommandException>());
        }

        [Test]
        public async Task ExecuteAsync_Logs_WhenCalled()
        {
            await _systemUnderTest.ExecuteAsync();

            _mockLoggigngProvider.ReceivedWithAnyArgs().OnCall(Arg.Any<string>(), Arg.Is<object>(o => o == null));
        }

        [Test]
        public async Task ExecuteAsync_Logs_WhenFinished()
        {
            await _systemUnderTest.ExecuteAsync();

            _mockLoggigngProvider.ReceivedWithAnyArgs().OnCallCompleted(Arg.Any<string>(), Arg.Is<object>(o => o == null));
        }
    }
}