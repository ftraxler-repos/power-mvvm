﻿using System;
using System.Threading.Tasks;
using FluentAssertions;
using NUnit.Framework;
using PowerMVVM.Commands;
using PowerMVVM.Commands.Testability;
using PowerMVVM.Tests.Commands.Mocks;

namespace PowerMVVM.Tests.Commands
{
    [TestFixture]
    public class ACommandTestabilityExtensionsTest
    {
        protected CommandMock Mock { get; private set; }

        protected ACommand SystemUnderTest { get; private set; }

        [SetUp]
        public void SetUp()
        {
            SystemUnderTest = Mock = new CommandMock();
        }
        
        [TestCase(true)]
        [TestCase(false)]
        public void CanExecute_ReturnsImplementation(bool canExecute)
        {
            Mock.TestProperty_CanExecute = canExecute;

            SystemUnderTest.CanExecute().Should().Be(canExecute);
        }

        [Theory]
        [TestCase(true)]
        [TestCase(false)]
        public void CanExecuteT_ReturnsImplementation(bool canExecute)
        {
            Mock.TestProperty_CanExecute = canExecute;

            SystemUnderTest.CanExecute(123).Should().Be(canExecute);
        }

        [Theory]
        [TestCase(true)]
        [TestCase(false)]
        public void CanExecuteT_ReturnsImplementation_UsesParameter(bool canExecute)
        {
            Mock.TestProperty_CanExecute = canExecute;

            const int PARAMETER = 123;
            SystemUnderTest.CanExecute(PARAMETER);
            Mock.AssertionProperty_PassedObject.Should().Be(PARAMETER);
        }

        [Test]
        public void Execute_RunsImplementation()
        {
            var taskWasRun = false;

            Task ExecutionTask()
            {
                taskWasRun = true;
                return Task.CompletedTask;
            }

            Mock.TestProperty_ExecutionTask = ExecutionTask();

            SystemUnderTest.ExecuteActionAsync();

            taskWasRun.Should().BeTrue();
        }

        [Test]
        public void ExecuteContinuationAction_CallsAction()
        {
            var continutionActionCalled = false;
            SystemUnderTest.ContinueWith(() => continutionActionCalled = true);

            SystemUnderTest.ExecuteContinuationAction();

            continutionActionCalled.Should().BeTrue();
        }

        [Test]
        public void ExecuteContinuationAction_ThrowsWhenNoActionSet()
        {
            SystemUnderTest.Invoking(sut => SystemUnderTest.ExecuteContinuationAction())
                           .Should()
                           .Throw<ArgumentNullException>();
        }

        [Test]
        public void ExecuteErrorHandler_CallsHandler()
        {
            var errorHandlerCalled = false;
            SystemUnderTest.SetErrorHandler(ex => errorHandlerCalled = true);

            SystemUnderTest.ExecuteErrorHandler(new ArgumentException());

            errorHandlerCalled.Should().BeTrue();
        }

        [Test]
        public void ExecuteErrorHandler_ThrowsWhenInvokedWithNoException()
        {
            var errorHandlerCalled = false;
            SystemUnderTest.SetErrorHandler(ex => errorHandlerCalled = true);

            SystemUnderTest.Invoking(sut => SystemUnderTest.ExecuteErrorHandler(null))
                           .Should()
                           .Throw<ArgumentNullException>();
            errorHandlerCalled.Should().BeFalse();
        }

        [Test]
        public void ExecuteErrorHandler_ThrowsWhenNoHandlerSet()
        {
            SystemUnderTest.Invoking(sut => SystemUnderTest.ExecuteErrorHandler(new ArgumentException()))
                           .Should()
                           .Throw<ArgumentNullException>();
        }

        [Test]
        public void ExecuteT_RunsImplementation()
        {
            var taskWasRun = false;

            Task ExecutionTask()
            {
                taskWasRun = true;
                return Task.CompletedTask;
            }

            Mock.TestProperty_ExecutionTask = ExecutionTask();

            SystemUnderTest.ExecuteActionAsync(123);

            taskWasRun.Should().BeTrue();
        }

        [Test]
        public void ExecuteT_RunsImplementation_UsesParameter()
        {
            Mock.TestProperty_ExecutionTask = Task.CompletedTask;

            const int PARAMETER = 123;
            SystemUnderTest.ExecuteActionAsync(PARAMETER);

            Mock.AssertionProperty_PassedObject.Should().Be(PARAMETER);
        }
    }
}