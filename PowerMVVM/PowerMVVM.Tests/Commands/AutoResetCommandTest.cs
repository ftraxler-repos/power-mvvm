﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;
using FluentAssertions;
using NUnit.Framework;
using PowerMVVM.Commands;
using PowerMVVM.Commands.Testability;
using PowerMVVM.Tests.Commands.TestHelper;

namespace PowerMVVM.Tests.Commands
{
    [TestFixture]
    public class AutoResetCommandTest
    {
        private AutoResetCommand _systemUnderTest;
        private bool _actionGotExecuted;

        [SetUp]
        public void Setup()
        {
            _actionGotExecuted = false;

            _systemUnderTest = new AutoResetCommand(() => { _actionGotExecuted = true; });
        }

        [Test]
        public async Task AlreadyExecutedCommand_DoesNotGetExecutedAgain()
        {
            _systemUnderTest.Signal();
            await _systemUnderTest.ExecuteAsync();
            _actionGotExecuted = false;
            await _systemUnderTest.ExecuteAsync();
            _actionGotExecuted.Should().BeFalse();
        }

        [Test]
        public async Task AlreadyExecutedCommand_HasCanExecute_False()
        {
            _systemUnderTest.Signal();
            await _systemUnderTest.ExecuteAsync();
            _systemUnderTest.CanExecute().Should().BeFalse();
        }

        [Test]
        public void AsICommand_AlreadyExecutedCommand_DoesNotGetExecutedAgain()
        {
            var sut = _systemUnderTest as ICommand;
            _systemUnderTest.Signal();
            sut.Execute(null);
            _actionGotExecuted = false;
            sut.Execute(null);
            _actionGotExecuted.Should().BeFalse();
        }

        [Test]
        public void AsICommand_AlreadyExecutedCommand_HasCanExecute_False()
        {
            var sut = _systemUnderTest as ICommand;
            _systemUnderTest.Signal();
            sut.Execute(null);
            _systemUnderTest.CanExecute().Should().BeFalse();
        }

        [Test]
        public void AsICommand_NewlyCreatedCommand_DoesNotGetExecuted()
        {
            var sut = _systemUnderTest as ICommand;
            sut.Execute(null);
            _actionGotExecuted.Should().BeFalse();
        }

        [Test]
        public void AsICommand_NewlyCreatedCommand_HasCanExecute_False()
        {
            var sut = _systemUnderTest as ICommand;
            sut.CanExecute(null).Should().BeFalse();
        }

        [Test]
        public void AsICommand_SignalledCommand_GetsExecuted()
        {
            _systemUnderTest.Signal();
            var sut = _systemUnderTest as ICommand;
            sut.Execute(null);
            _actionGotExecuted.Should().BeTrue();
        }

        [Test]
        public void AsICommand_SignalledCommand_HasCanExecute_True()
        {
            var sut = _systemUnderTest as ICommand;
            _systemUnderTest.Signal();
            sut.CanExecute(null).Should().BeTrue();
        }

        [Test]
        public void Constructor_ThrowsWhenActionIsNull()
        {
            Action action = null;
            // ReSharper disable once AssignNullToNotNullAttribute - Testing Null Handling
            Assert.That(() => new AutoResetCommand(action), Throws.ArgumentNullException);
        }

        [Test]
        public void Constructor_ThrowsWhenAsyncActionIsNull()
        {
            Func<Task> action = null;
            // ReSharper disable once AssignNullToNotNullAttribute - Testing Null Handling
            Assert.That(() => new AutoResetCommand(action), Throws.ArgumentNullException);
        }

        [Test]
        public void ExecuteContinuationAction_CallsAction()
        {
            var continutionActionCalled = false;
            _systemUnderTest.ContinueWith(() => continutionActionCalled = true);

            _systemUnderTest.ExecuteContinuationAction();

            continutionActionCalled.Should().BeTrue();
        }

        [Test]
        public void ExecuteContinuationAction_ThrowsWhenNoActionSet()
        {
            _systemUnderTest.Invoking(sut => _systemUnderTest.ExecuteContinuationAction())
                .Should()
                .Throw<ArgumentNullException>();
        }

        [Test]
        public void ExecuteErrorHandler_CallsHandler()
        {
            var errorHandlerCalled = false;
            _systemUnderTest.SetErrorHandler(ex => errorHandlerCalled = true);

            _systemUnderTest.ExecuteErrorHandler(new ArgumentException());

            errorHandlerCalled.Should().BeTrue();
        }

        [Test]
        public void ExecuteErrorHandler_ThrowsWhenInvokedWithNoException()
        {
            var errorHandlerCalled = false;
            _systemUnderTest.SetErrorHandler(ex => errorHandlerCalled = true);

            _systemUnderTest.Invoking(sut => _systemUnderTest.ExecuteErrorHandler(null))
                .Should()
                .Throw<ArgumentNullException>();
            errorHandlerCalled.Should().BeFalse();
        }

        [Test]
        public void ExecuteErrorHandler_ThrowsWhenNoHandlerSet()
        {
            _systemUnderTest.Invoking(sut => _systemUnderTest.ExecuteErrorHandler(new ArgumentException()))
                .Should()
                .Throw<ArgumentNullException>();
        }

        [Test]
        public async Task NewlyCreatedCommand_DoesNotGetExecuted()
        {
            await _systemUnderTest.ExecuteAsync();
            _actionGotExecuted.Should().BeFalse();
        }

        [Test]
        public void NewlyCreatedCommand_HasCanExecute_False()
        {
            _systemUnderTest.CanExecute().Should().BeFalse();
        }

        [Test]
        public async Task SignalledCommand_GetsExecuted()
        {
            _systemUnderTest.Signal();
            await _systemUnderTest.ExecuteAsync();
            _actionGotExecuted.Should().BeTrue();
        }

        [Test]
        public void SignalledCommand_HasCanExecute_True()
        {
            _systemUnderTest.Signal();
            _systemUnderTest.CanExecute().Should().BeTrue();
        }

        [Test]
        public void Testability_CanExecute_WithParameter_Throws()
        {
            _systemUnderTest.Invoking(sut => sut.CanExecute(new object()))
                .Should()
                .Throw<CommandTestException>();
        }

        [Test]
        public async Task Testability_ExecuteActionAsync_DoesIgnoreFlagAndExecutes()
        {
            await _systemUnderTest.ExecuteActionAsync();
            _actionGotExecuted.Should().BeTrue();
        }

        [Test]
        public async Task Testability_ExecuteActionAsync_WithParameter_Throws()
        {
            await _systemUnderTest.Awaiting(sut => sut.ExecuteActionAsync(new object()))
                .Should()
                .ThrowAsync<CommandTestException>();
        }
    }
}