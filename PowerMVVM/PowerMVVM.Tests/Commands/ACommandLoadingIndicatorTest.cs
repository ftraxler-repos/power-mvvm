﻿using System.Threading.Tasks;
using NSubstitute;
using NUnit.Framework;
using PowerMVVM.Commands;
using PowerMVVM.Commands.CommandSettings;
using PowerMVVM.Tests.Commands.TestHelper;

namespace PowerMVVM.Tests.Commands
{
    [TestFixture]
    public class ACommandLoadingIndicatorTest
    {
        [Test]
        public async Task CustomIndicator_DoesNotCallStatusProvider()
        {
            Command.StatusProvider = Substitute.For<AStatusProvider>();
            var mockIndicator = Substitute.For<ILoadingIndicator>();
            var command = Command.Create(() => { }).UseLoadingIndicator(mockIndicator);

            await command.ExecuteAsync();

            await Command.StatusProvider.DidNotReceive().ShowLoadingIndicatorAsync(true);
            await Command.StatusProvider.DidNotReceive().ShowLoadingIndicatorAsync(false);
            await Command.StatusProvider.DidNotReceive().HideLoadingIndicatorAsync(true);
            await Command.StatusProvider.DidNotReceive().HideLoadingIndicatorAsync(false);
        }

        [Test]
        public async Task CustomIndicator_Hide_GetsCalled()
        {
            var mockIndicator = Substitute.For<ILoadingIndicator>();
            var command = Command.Create(() => { }).UseLoadingIndicator(mockIndicator);

            await command.ExecuteAsync();

            await mockIndicator.Received().HideLoadingAsync();
        }

        [Test]
        public async Task CustomIndicator_Show_GetsCalled()
        {
            var mockIndicator = Substitute.For<ILoadingIndicator>();
            var command = Command.Create(() => { }).UseLoadingIndicator(mockIndicator);

            await command.ExecuteAsync();

            await mockIndicator.Received().ShowLoadingAsync(Arg.Any<string>());
        }

        [Test]
        public void CustomIndicator_ThrowsWhenNull()
        {
            Assert.That(() => Command.Create(() => { }).UseLoadingIndicator(null!), Throws.ArgumentNullException);
        }

        [Test]
        public async Task CustomIndicatorFunction_Hide_GetsCalled()
        {
            var called = false;
            var command = Command.Create(() => { })
                .UseLoadingIndicator(() => Task.CompletedTask, () =>
                {
                    called = true;
                    return Task.CompletedTask;
                });

            await command.ExecuteAsync();

            Assert.That(called, Is.True);
        }

        [Test]
        public async Task CustomIndicatorFunction_Show_GetsCalled()
        {
            var called = false;
            var command = Command.Create(() => { })
                .UseLoadingIndicator(() =>
                {
                    called = true;
                    return Task.CompletedTask;
                }, () => Task.CompletedTask);

            await command.ExecuteAsync();

            Assert.That(called, Is.True);
        }
    }
}