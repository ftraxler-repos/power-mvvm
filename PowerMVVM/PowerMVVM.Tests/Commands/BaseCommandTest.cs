﻿using System.Threading.Tasks;
using FluentAssertions;
using NUnit.Framework;
using PowerMVVM.Commands;
using PowerMVVM.Tests.Commands.Mocks;

namespace PowerMVVM.Tests.Commands
{
    [TestFixture]
    public class BaseCommandTest
    {
        protected CommandMock Mock { get; private set; }

        protected ACommand SystemUnderTest { get; private set; }

        [SetUp]
        public void SetUp()
        {
            SystemUnderTest = Mock = new CommandMock();
        }
        
        [Test]
        public void RaiseCanExecuteChanged_InvokesEvent()
        {
            var eventWasCalled = false;
            SystemUnderTest.CanExecuteChanged += (s, e) => eventWasCalled = true;

            SystemUnderTest.RaiseCanExecuteChanged();

            eventWasCalled.Should().BeTrue();
        }

        [Test]
        public async Task RaiseCanExecuteChanged_InvokesEvent_FromOtherTask()
        {
            var eventWasCalled = false;
            SystemUnderTest.CanExecuteChanged += (s, e) => eventWasCalled = true;

            await Task.Factory.StartNew(() =>
                SystemUnderTest.RaiseCanExecuteChanged());

            await Task.Delay(10);
            eventWasCalled.Should().BeTrue();
        }
    }
}