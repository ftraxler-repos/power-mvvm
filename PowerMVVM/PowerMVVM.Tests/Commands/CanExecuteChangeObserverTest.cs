﻿using System;
using System.ComponentModel;
using FluentAssertions;
using NSubstitute;
using NUnit.Framework;
using PowerMVVM.Commands;
using PowerMVVM.Tests.Commands.Mocks;

namespace PowerMVVM.Tests.Commands
{
    [TestFixture]
    public class CanExecuteChangeObserverTest
    {
        private CanExecuteChangeObserver _systemUnderTest;
        private int _canExecuteChangeCalled;
        private INotifyPropertyChanged _mockViewModel;

        [SetUp]
        public void SetUp()
        {
            var mockCommand = new CommandMock();
            _canExecuteChangeCalled = 0;
            mockCommand.CanExecuteChanged += (o, e) => _canExecuteChangeCalled++;
            _systemUnderTest = new CanExecuteChangeObserver(mockCommand);

            _mockViewModel = Substitute.For<INotifyPropertyChanged>();
        }

        [Test]
        public void DoesNotRaiseCanExecuteChanged_WhenConfigured_ButNothingSubscribed()
        {
            const string PROPERTY_NAME = "DummyProperty";
            _systemUnderTest.SetPropertyHolder(_mockViewModel);

            _mockViewModel.PropertyChanged +=
                Raise.Event<PropertyChangedEventHandler>(this, new PropertyChangedEventArgs(PROPERTY_NAME));

            _canExecuteChangeCalled.Should().Be(0);
        }

        [Test]
        public void DoesNotRaiseCanExecuteChanged_WhenNotConfigured_ButSubscribed()
        {
            const string PROPERTY_NAME = "DummyProperty";
            _systemUnderTest.SubscribeToProperty(PROPERTY_NAME);

            _mockViewModel.PropertyChanged +=
                Raise.Event<PropertyChangedEventHandler>(this, new PropertyChangedEventArgs(PROPERTY_NAME));

            _canExecuteChangeCalled.Should().Be(0);
        }

        [Test]
        public void DoesNotRaiseCanExecuteChanged_WhenSubscribedAndConfigured_FinallyCleared()
        {
            const string PROPERTY_NAME_1 = "DummyProperty1";
            const string PROPERTY_NAME_2 = "DummyProperty2";
            _systemUnderTest.SubscribeToProperty(PROPERTY_NAME_1)
                .SubscribeToProperty(PROPERTY_NAME_2)
                .SetPropertyHolder(_mockViewModel)
                .Clear();

            _mockViewModel.PropertyChanged +=
                Raise.Event<PropertyChangedEventHandler>(this, new PropertyChangedEventArgs(PROPERTY_NAME_1));
            _canExecuteChangeCalled.Should().Be(0);

            _mockViewModel.PropertyChanged +=
                Raise.Event<PropertyChangedEventHandler>(this, new PropertyChangedEventArgs(PROPERTY_NAME_2));
            _canExecuteChangeCalled.Should().Be(0);
        }

        [Test]
        public void RaisesCanExecuteChanged_WhenCleared_ThenConfiguredAndSubscribed()
        {
            const string PROPERTY_NAME = "DummyProperty";
            _systemUnderTest.Clear().SetPropertyHolder(_mockViewModel).SubscribeToProperty(PROPERTY_NAME);

            _mockViewModel.PropertyChanged +=
                Raise.Event<PropertyChangedEventHandler>(this, new PropertyChangedEventArgs(PROPERTY_NAME));
            _canExecuteChangeCalled.Should().Be(1);
        }

        [Test]
        public void RaisesCanExecuteChanged_WhenCleared_ThenSubscribedAndConfigured()
        {
            const string PROPERTY_NAME = "DummyProperty";
            _systemUnderTest.Clear().SubscribeToProperty(PROPERTY_NAME).SetPropertyHolder(_mockViewModel);

            _mockViewModel.PropertyChanged +=
                Raise.Event<PropertyChangedEventHandler>(this, new PropertyChangedEventArgs(PROPERTY_NAME));
            _canExecuteChangeCalled.Should().Be(1);
        }

        [Test]
        public void RaisesCanExecuteChanged_WhenConfigured_AndSubscribed()
        {
            const string PROPERTY_NAME = "DummyProperty";
            _systemUnderTest.SetPropertyHolder(_mockViewModel).SubscribeToProperty(PROPERTY_NAME);

            _mockViewModel.PropertyChanged +=
                Raise.Event<PropertyChangedEventHandler>(this, new PropertyChangedEventArgs(PROPERTY_NAME));

            _canExecuteChangeCalled.Should().Be(1);
        }

        [Test]
        public void RaisesCanExecuteChanged_WhenConfigured_AndSubscribed_IsCaseSensitive()
        {
            const string PROPERTY_NAME = "DummyProperty";
            _systemUnderTest.SetPropertyHolder(_mockViewModel).SubscribeToProperty(PROPERTY_NAME);

            _mockViewModel.PropertyChanged +=
                Raise.Event<PropertyChangedEventHandler>(this, new PropertyChangedEventArgs("duMmyProPertY"));

            _canExecuteChangeCalled.Should().Be(0);
        }

        [Test]
        public void RaisesCanExecuteChanged_WhenConfigured_AndSubscribed_ThenReconfigured()
        {
            const string PROPERTY_NAME = "DummyProperty";
            var initialviewModel = Substitute.For<INotifyPropertyChanged>();
            _systemUnderTest.SetPropertyHolder(initialviewModel).SubscribeToProperty(PROPERTY_NAME).SetPropertyHolder(_mockViewModel);

            _mockViewModel.PropertyChanged +=
                Raise.Event<PropertyChangedEventHandler>(this, new PropertyChangedEventArgs(PROPERTY_NAME));

            _canExecuteChangeCalled.Should().Be(1);
        }

        [Test]
        public void RaisesCanExecuteChanged_WhenConfigured_AndSubscribedToTwoProperties()
        {
            const string PROPERTY_NAME_1 = "DummyProperty1";
            const string PROPERTY_NAME_2 = "DummyProperty2";
            _systemUnderTest.SetPropertyHolder(_mockViewModel).SubscribeToProperty(PROPERTY_NAME_1).SubscribeToProperty(PROPERTY_NAME_2);

            _mockViewModel.PropertyChanged +=
                Raise.Event<PropertyChangedEventHandler>(this, new PropertyChangedEventArgs(PROPERTY_NAME_1));
            _canExecuteChangeCalled.Should().Be(1);

            _mockViewModel.PropertyChanged +=
                Raise.Event<PropertyChangedEventHandler>(this, new PropertyChangedEventArgs(PROPERTY_NAME_2));
            _canExecuteChangeCalled.Should().Be(2);
        }

        [Test]
        public void RaisesCanExecuteChanged_WhenConfiguredAndUnsubscribed_OnOnlyLeftProperty()
        {
            const string PROPERTY_NAME_1 = "DummyProperty1";
            const string PROPERTY_NAME_2 = "DummyProperty2";
            _systemUnderTest.SetPropertyHolder(_mockViewModel)
                .SubscribeToProperty(PROPERTY_NAME_1)
                .SubscribeToProperty(PROPERTY_NAME_2)
                .UnsubscribeProperty(PROPERTY_NAME_1);

            _mockViewModel.PropertyChanged +=
                Raise.Event<PropertyChangedEventHandler>(this, new PropertyChangedEventArgs(PROPERTY_NAME_1));
            _canExecuteChangeCalled.Should().Be(0);

            _mockViewModel.PropertyChanged +=
                Raise.Event<PropertyChangedEventHandler>(this, new PropertyChangedEventArgs(PROPERTY_NAME_2));
            _canExecuteChangeCalled.Should().Be(1);
        }

        [Test]
        public void RaisesCanExecuteChanged_WhenSubscribedToTwoProperties_AndThenConfigured()
        {
            const string PROPERTY_NAME_1 = "DummyProperty1";
            const string PROPERTY_NAME_2 = "DummyProperty2";
            _systemUnderTest.SubscribeToProperty(PROPERTY_NAME_1).SubscribeToProperty(PROPERTY_NAME_2).SetPropertyHolder(_mockViewModel);

            _mockViewModel.PropertyChanged +=
                Raise.Event<PropertyChangedEventHandler>(this, new PropertyChangedEventArgs(PROPERTY_NAME_1));
            _canExecuteChangeCalled.Should().Be(1);

            _mockViewModel.PropertyChanged +=
                Raise.Event<PropertyChangedEventHandler>(this, new PropertyChangedEventArgs(PROPERTY_NAME_2));
            _canExecuteChangeCalled.Should().Be(2);
        }

        [Test]
        public void SetPropertyHolder_ThrowsWhenNull()
        {
            _systemUnderTest.Invoking(sut => _systemUnderTest.SetPropertyHolder(null!))
                .Should()
                .Throw<ArgumentNullException>();
        }

        [Test]
        public void SubscribeToProperty_ThrowsWhenNameIsAlreadyRegistered()
        {
            const string PROPERTY_NAME = "DummyProperty";
            _systemUnderTest.SubscribeToProperty(PROPERTY_NAME);
            _systemUnderTest.Invoking(sut => _systemUnderTest.SubscribeToProperty(PROPERTY_NAME))
                .Should()
                .Throw<ArgumentException>();
        }

        [Test]
        public void SubscribeToProperty_ThrowsWhenNameIsEmpty()
        {
            _systemUnderTest.Invoking(sut => _systemUnderTest.SubscribeToProperty(string.Empty))
                .Should()
                .Throw<ArgumentNullException>();
        }

        [Test]
        public void SubscribeToProperty_ThrowsWhenNameIsNull()
        {
            _systemUnderTest.Invoking(sut => _systemUnderTest.SubscribeToProperty(null))
                .Should()
                .Throw<ArgumentNullException>();
        }

        [Test]
        public void UnsubscribeProperty_ThrowsWhenNameIsEmpty()
        {
            _systemUnderTest.Invoking(sut => _systemUnderTest.UnsubscribeProperty(string.Empty))
                .Should()
                .Throw<ArgumentNullException>();
        }

        [Test]
        public void UnsubscribeProperty_ThrowsWhenNameIsNotRegistered()
        {
            const string PROPERTY_NAME = "DummyProperty";
            _systemUnderTest.Invoking(sut => _systemUnderTest.UnsubscribeProperty(PROPERTY_NAME))
                .Should()
                .Throw<ArgumentException>();
        }

        [Test]
        public void UnsubscribeProperty_ThrowsWhenNameIsNull()
        {
            _systemUnderTest.Invoking(sut => _systemUnderTest.UnsubscribeProperty(null))
                .Should()
                .Throw<ArgumentNullException>();
        }
    }
}