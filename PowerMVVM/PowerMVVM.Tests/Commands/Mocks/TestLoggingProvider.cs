﻿#nullable enable
using System;
using PowerMVVM.Commands.CommandSettings;

// ReSharper disable InconsistentNaming
namespace PowerMVVM.Tests.Commands.Mocks
{
    public class TestLoggingProvider : ILoggingProvider
    {
        public string? AssertionProperty_CommandName { get; private set; }

        public object? AssertionProperty_CommandParameter { get; private set; }

        /// <inheritdoc />
        public void OnCall(string commandName, object? parameter)
        {
            AssertionProperty_CommandName = commandName;
            AssertionProperty_CommandParameter = parameter;
        }

        /// <inheritdoc />
        public void OnCallCompleted(string commandName, object? parameter)
        {
            AssertionProperty_CommandName = commandName;
            AssertionProperty_CommandParameter = parameter;
        }

        /// <inheritdoc />
        public void OnException(Exception exception) { }
    }
}