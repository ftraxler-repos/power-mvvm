﻿using System.Threading.Tasks;
using PowerMVVM.Commands;

// ReSharper disable InconsistentNaming - Better Readbility for Mock
namespace PowerMVVM.Tests.Commands.Mocks
{
    public class CommandMock : ACommand
    {
        /// <inheritdoc />
        public CommandMock() : base(nameof(CommandMock)) { }

        public int AssertionProperty_CanExecuteInternalCalled { get; private set; }

        public int AssertionProperty_ExecuteInternalCalled { get; private set; }

        public bool AssertionProperty_InternalConfigureAwait => GetConfigureAwait();

        public object AssertionProperty_PassedObject { get; private set; }

        public bool TestProperty_CanExecute { get; set; }

        public Task TestProperty_ExecutionTask { get; set; }

        /// <inheritdoc />
        protected override bool CanExecuteInternal(object parameter)
        {
            AssertionProperty_CanExecuteInternalCalled++;
            AssertionProperty_PassedObject = parameter;
            return TestProperty_CanExecute;
        }

        /// <inheritdoc />
        protected override Task ExecuteInternalAsync(object parameter)
        {
            AssertionProperty_ExecuteInternalCalled++;
            AssertionProperty_PassedObject = parameter;
            return TestProperty_ExecutionTask;
        }
    }
}