﻿namespace PowerMVVM.Tests.Commands.Mocks
{
    public class DummyClass { }

    public class DummyBase { }

    public class DummyInherited : DummyBase { }

    public struct DummyStruct { }
}