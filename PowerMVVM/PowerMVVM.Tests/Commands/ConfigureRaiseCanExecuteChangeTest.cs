﻿using System.Threading.Tasks;
using FluentAssertions;
using NUnit.Framework;
using PowerMVVM.Commands;
using PowerMVVM.Tests.Commands.Mocks;
using PowerMVVM.Tests.Commands.TestHelper;

namespace PowerMVVM.Tests.Commands
{
    [TestFixture]
    [UsesCommandSettings]
    public class ConfigureRaiseCanExecuteChangeTest
    {
        public ACommand SystemUnderTest { get; private set; }

        [SetUp]
        public void SetUp()
        {
            SystemUnderTest = new CommandMock
            {
                TestProperty_ExecutionTask = Task.CompletedTask
            };
        }

        [Test]
        public void ConfigureRaiseCanExecuteChange_ProvidesAlwaysSameInstance()
        {
            IPropertyChangeObserver observer1 = null;
            IPropertyChangeObserver observer2 = null;
            SystemUnderTest.ConfigureRaiseCanExecuteChanged(pco =>
            {
                observer1 = pco;
                return pco;
            });
            SystemUnderTest.ConfigureRaiseCanExecuteChanged(pco =>
            {
                observer2 = pco;
                return pco;
            });
            observer1.Should().BeSameAs(observer2);
        }

        [Test]
        public void ConfigureRaiseCanExecuteChange_ProvidesPropertyChangeObserverInstance()
        {
            IPropertyChangeObserver observer = null;
            SystemUnderTest.ConfigureRaiseCanExecuteChanged(pco => observer = pco);
            observer.Should().NotBeNull();
        }
    }
}