﻿using System;
using System.Threading;
using System.Threading.Tasks;
using FluentAssertions;
using NUnit.Framework;
using PowerMVVM.Commands;
using PowerMVVM.Commands.Testability;

namespace PowerMVVM.Tests.Commands
{
    [TestFixture]
    public class BackgroundCommandTestabilityExtensionsImplementationTest
    {
        [TestCase(true)]
        [TestCase(false)]
        public void CanExecute_ReturnsImplementation(bool canExecute)
        {
            Task MockAction(object o, CancellationToken ct)
            {
                return Task.Delay(1, ct);
            }

            bool MockCanExecute(object o)
            {
                return canExecute;
            }

            var systemUnderTest = new BackgroundCommand(MockAction, MockCanExecute);

            systemUnderTest.CanExecute().Should().Be(canExecute);
        }

        [Theory]
        [TestCase(true)]
        [TestCase(false)]
        public void CanExecuteT_ReturnsImplementation(bool canExecute)
        {
            Task MockAction(object o, CancellationToken ct)
            {
                return Task.Delay(1, ct);
            }

            bool MockCanExecute(object o)
            {
                return canExecute;
            }

            var systemUnderTest = new BackgroundCommand(MockAction, MockCanExecute);

            systemUnderTest.CanExecute(123).Should().Be(canExecute);
        }

        [Test]
        public void Execute_RunsImplementation()
        {
            var taskWasRun = false;

            Task MockAction(object o, CancellationToken ct)
            {
                taskWasRun = true;
                return Task.CompletedTask;
            }

            bool MockCanExecute(object o)
            {
                return true;
            }

            var systemUnderTest = new BackgroundCommand(MockAction, MockCanExecute);

            systemUnderTest.ExecuteActionAsync(CancellationToken.None);

            taskWasRun.Should().BeTrue();
        }

        [Test]
        public void ExecuteCancellationAction_CallsHandler()
        {
            var cancellationHandlerCalled = false;

            Task MockAction(object o, CancellationToken ct)
            {
                return Task.CompletedTask;
            }

            bool MockCanExecute(object o)
            {
                return true;
            }

            var systemUnderTest = new BackgroundCommand(MockAction, MockCanExecute);

            systemUnderTest.SetCancellationHandler(() => cancellationHandlerCalled = true);

            systemUnderTest.ExecuteCancellationAction();

            cancellationHandlerCalled.Should().BeTrue();
        }

        [Test]
        public void ExecuteCancellationAction_ThrowsWhenNoHandlerSet()
        {
            Task MockAction(object o, CancellationToken ct)
            {
                return Task.CompletedTask;
            }

            bool MockCanExecute(object o)
            {
                return true;
            }

            var systemUnderTest = new BackgroundCommand(MockAction, MockCanExecute);

            systemUnderTest.Invoking(sut => systemUnderTest.ExecuteCancellationAction())
                .Should()
                .Throw<ArgumentNullException>();
        }

        [Test]
        public void ExecuteCancellationTokenFactoryMethod_CallsFactory()
        {
            Task MockAction(object o, CancellationToken ct)
            {
                return Task.CompletedTask;
            }

            bool MockCanExecute(object o)
            {
                return true;
            }

            var systemUnderTest = new BackgroundCommand(MockAction, MockCanExecute);
            var token = new CancellationToken();
            systemUnderTest.RegisterCancellationTokenFactory(() => token);

            var factoryToken = systemUnderTest.ExecuteCancellationTokenFactoryMethod();

            factoryToken.Should().Be(token);
        }

        [Test]
        public void ExecuteCancellationTokenFactoryMethod_ThrowsWhenNoActionSet()
        {
            Task MockAction(object o, CancellationToken ct)
            {
                return Task.CompletedTask;
            }

            bool MockCanExecute(object o)
            {
                return true;
            }

            var systemUnderTest = new BackgroundCommand(MockAction, MockCanExecute);

            systemUnderTest.Invoking(sut => systemUnderTest.ExecuteCancellationTokenFactoryMethod())
                .Should()
                .Throw<ArgumentNullException>();
        }

        [Test]
        public void ExecuteContinuationAction_CallsAction()
        {
            var continutionActionCalled = false;

            Task MockAction(object o, CancellationToken ct)
            {
                return Task.CompletedTask;
            }

            bool MockCanExecute(object o)
            {
                return true;
            }

            var systemUnderTest = new BackgroundCommand(MockAction, MockCanExecute);

            systemUnderTest.ContinueWith(o => continutionActionCalled = true);

            systemUnderTest.ExecuteContinuationAction();

            continutionActionCalled.Should().BeTrue();
        }

        [Test]
        public void ExecuteContinuationAction_ThrowsWhenNoActionSet()
        {
            Task MockAction(object o, CancellationToken ct)
            {
                return Task.CompletedTask;
            }

            bool MockCanExecute(object o)
            {
                return true;
            }

            var systemUnderTest = new BackgroundCommand(MockAction, MockCanExecute);

            systemUnderTest.Invoking(sut => systemUnderTest.ExecuteContinuationAction())
                .Should()
                .Throw<ArgumentNullException>();
        }

        [Test]
        public void ExecuteErrorHandler_CallsHandler()
        {
            var errorHandlerCalled = false;

            Task MockAction(object o, CancellationToken ct)
            {
                return Task.CompletedTask;
            }

            bool MockCanExecute(object o)
            {
                return true;
            }

            var systemUnderTest = new BackgroundCommand(MockAction, MockCanExecute);

            systemUnderTest.SetErrorHandler(ex => errorHandlerCalled = true);

            systemUnderTest.ExecuteErrorHandler(new ArgumentException());

            errorHandlerCalled.Should().BeTrue();
        }

        [Test]
        public void ExecuteErrorHandler_ThrowsWhenInvokedWithNoException()
        {
            Task MockAction(object o, CancellationToken ct)
            {
                return Task.CompletedTask;
            }

            bool MockCanExecute(object o)
            {
                return true;
            }

            var systemUnderTest = new BackgroundCommand(MockAction, MockCanExecute);

            var errorHandlerCalled = false;
            systemUnderTest.SetErrorHandler(ex => errorHandlerCalled = true);

            systemUnderTest.Invoking(sut => systemUnderTest.ExecuteErrorHandler(null!))
                .Should()
                .Throw<ArgumentNullException>();
            errorHandlerCalled.Should().BeFalse();
        }

        [Test]
        public void ExecuteErrorHandler_ThrowsWhenNoHandlerSet()
        {
            Task MockAction(object o, CancellationToken ct)
            {
                return Task.CompletedTask;
            }

            bool MockCanExecute(object o)
            {
                return true;
            }

            var systemUnderTest = new BackgroundCommand(MockAction, MockCanExecute);

            systemUnderTest.Invoking(sut => systemUnderTest.ExecuteErrorHandler(new ArgumentException()))
                .Should()
                .Throw<ArgumentNullException>();
        }

        [Test]
        public void ExecuteT_RunsImplementation()
        {
            var taskWasRun = false;

            Task MockAction(object o, CancellationToken ct)
            {
                taskWasRun = true;
                return Task.CompletedTask;
            }

            bool MockCanExecute(object o)
            {
                return true;
            }

            var systemUnderTest = new BackgroundCommand(MockAction, MockCanExecute);

            systemUnderTest.ExecuteActionAsync(CancellationToken.None, 123);

            taskWasRun.Should().BeTrue();
        }
    }
}