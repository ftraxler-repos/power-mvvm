﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;
using FluentAssertions;
using NSubstitute;
using NUnit.Framework;
using PowerMVVM.Commands;
using PowerMVVM.Commands.Testability;
using PowerMVVM.Tests.Commands.Mocks;
using PowerMVVM.Tests.Commands.TestHelper;
// ReSharper disable ObjectCreationAsStatement - Unit Tests

namespace PowerMVVM.Tests.Commands
{
    [TestFixture]
    public class WrappedCommandTest
    {
        [TestCase(true)]
        [TestCase(false)]
        public void CanExecute_ReturnsCorrectValue(bool canExecute)
        {
            var command = Substitute.For<ICommand>();
            command.CanExecute(Arg.Any<object>()).Returns(canExecute);
            var sut = new WrappedCommand(command);

            sut.CanExecute().Should().Be(canExecute);
        }

        [Test]
        public void CanExecute_ExecutesPassedFunction_WithNullParameter()
        {
            var command = Substitute.For<ICommand>();
            var sut = new WrappedCommand(command);

            sut.CanExecute();
            command.Received().CanExecute(null);
        }

        [Test]
        public void CanExecute_ExecutesPassedFunction_WithParameter()
        {
            var command = Substitute.For<ICommand>();
            var sut = new WrappedCommand(command);
            var dummyObject = new object();

            sut.CanExecute(dummyObject);
            command.Received().CanExecute(dummyObject);
        }

        [Test]
        public void Constructor_ThrowsWhenCommandIsNull()
        {
            Assert.Throws<ArgumentNullException>(() =>
                new WrappedCommand(null!));
        }

        [Test]
        public async Task Execute_ExecutesAction_WithNullParameter()
        {
            var command = Substitute.For<ICommand>();
            var sut = new WrappedCommand(command);

            await sut.ExecuteActionAsync();
            command.Received().Execute(null);
        }

        [Test]
        public async Task Execute_ExecutesAction_WithParameter()
        {
            var command = Substitute.For<ICommand>();
            var sut = new WrappedCommand(command);
            var dummyParameter = new object();

            await sut.ExecuteActionAsync(dummyParameter);
            command.Received().Execute(dummyParameter);
        }

        [Test]
        [UsesCommandSettings]
        public async Task LoggedCommandName_IsCommandToString()
        {
            var command = Substitute.For<ICommand>();
            var mockLoggingProvider = new TestLoggingProvider();
            Command.LoggingProvider = mockLoggingProvider;
            var sut = new WrappedCommand(command, command.ToString()!);
            await sut.ExecuteAsync();

            mockLoggingProvider.AssertionProperty_CommandName.Should().Be(command.ToString());
        }
    }
}