﻿using System;
using System.Threading;
using System.Threading.Tasks;
using FluentAssertions;
using NUnit.Framework;
using PowerMVVM.Commands;
using PowerMVVM.Commands.Testability;
using PowerMVVM.Tests.Commands.Mocks;
using PowerMVVM.Tests.Commands.TestHelper;
// ReSharper disable ObjectCreationAsStatement - Unit Tests

namespace PowerMVVM.Tests.Commands
{
    [TestFixture]
    public class BackgroundCommandTest
    {
        [TestCase(true)]
        [TestCase(false)]
        public void CanExecute_ReturnsCorrectValue(bool canExecute)
        {
            var sut = new BackgroundCommand((o, ct) => Task.Delay(1, ct), o => canExecute);

            sut.CanExecute().Should().Be(canExecute);
        }

        private Task DummyAction(object o, CancellationToken ct)
        {
            return Task.Delay(1, ct);
        }

        [Test]
        public void CanExecute_ExecutesPassedFunction()
        {
            var canExecuteCalled = false;
            var sut = new BackgroundCommand((o, ct) => Task.Delay(1, ct), o =>
            {
                canExecuteCalled = true;
                return true;
            });

            sut.CanExecute();
            canExecuteCalled.Should().BeTrue();
        }

        [Test]
        public void Constructor_ThrowsWhenActionIsNull()
        {
            Assert.Throws<ArgumentNullException>(() =>
                new BackgroundCommand(null!, o => true));
        }

        [Test]
        public void Constructor_ThrowsWhenCanExecuteIsNull()
        {
            Assert.Throws<ArgumentNullException>(() =>
                new BackgroundCommand((o, ct) => Task.Delay(1, ct), null!));
        }

        [Test]
        public async Task Execute_ExecutesAction()
        {
            var actionCalled = false;
            var sut = new BackgroundCommand(async (o, ct) =>
            {
                actionCalled = true;
                await Task.Delay(1, ct);
            }, o => true);

            await sut.ExecuteActionAsync(CancellationToken.None);
            actionCalled.Should().BeTrue();
        }

        [Test]
        public async Task Execute_ExecutesAction_WithParameter()
        {
            var dummyObject = new object();
            object passedObject = null!;
            var sut = new BackgroundCommand(async (o, ct) =>
            {
                passedObject = o;
                await Task.Delay(1, ct);
            }, o => true);

            await sut.ExecuteActionAsync(CancellationToken.None, dummyObject);
            passedObject.Should().Be(dummyObject);
        }

        [Test]
        [UsesCommandSettings]
        public async Task LoggedCommandName_IsNameOfAction()
        {
            var mockLoggingProvider = new TestLoggingProvider();
            Command.LoggingProvider = mockLoggingProvider;
            var sut = new BackgroundCommand(DummyAction, o => true);
            await sut.ExecuteAsync();

            mockLoggingProvider.AssertionProperty_CommandName.Should().Be(nameof(DummyAction));
        }
    }
}