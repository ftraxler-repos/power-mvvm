﻿using System.Threading;
using System.Threading.Tasks;
using FluentAssertions;
using NUnit.Framework;
using PowerMVVM.Commands;
using PowerMVVM.Tests.Commands.TestHelper;

namespace PowerMVVM.Tests.Commands
{
    [TestFixture]
    [NonParallelizable]
    public class BackgroundCommandLockTest
    {
        [Test]
        public async Task StartingMultipleTimes_LocksUntilPreviousTaskFinished()
        {
            Task MockAction(object o, CancellationToken ct)
            {
                return Task.Delay(10, ct);
            }

            bool MockCanExecute(object o)
            {
                return true;
            }

            var systemUnderTest = new BackgroundCommand(MockAction, MockCanExecute);
            
            int callCounter = 0;
            systemUnderTest.ContinueWith(o => callCounter++);

            await systemUnderTest.ExecuteAsync();
            callCounter.Should().Be(0);
            await systemUnderTest.ExecuteAsync();

            await Task.Delay(5);
            callCounter.Should().Be(1);
            await Task.Delay(10);
            callCounter.Should().Be(2);
        }
        
        [Test]
        public async Task StartingMultipleTimes_Cancelling_ResetsLock()
        {
            int callCounter = 0;
            Task MockAction(object o, CancellationToken ct)
            {
                callCounter++;
                return Task.Delay(10, ct);
            }
        
            bool MockCanExecute(object o)
            {
                return true;
            }
        
            var systemUnderTest = new BackgroundCommand(MockAction, MockCanExecute);
            
            int tokenCounter = -1;
            var cancellationTokenSources = new CancellationTokenSource[2];
            cancellationTokenSources[0] = new CancellationTokenSource();
            cancellationTokenSources[1] = new CancellationTokenSource();
            systemUnderTest.RegisterCancellationTokenFactory(() =>
            {
                tokenCounter++;
                return cancellationTokenSources[tokenCounter].Token;
            });
        
            cancellationTokenSources[0].Cancel();
            cancellationTokenSources[1].CancelAfter(5);
            
            await systemUnderTest.ExecuteAsync();
            callCounter.Should().Be(0);
            await systemUnderTest.ExecuteAsync();
            await Task.Delay(10);
            callCounter.Should().Be(1);
        }
        
        [Test]
        public async Task StartingMultipleTimes_CancellingBothDuringRuntime_ResetsLock()
        {
            int callCounter = 0;
            Task MockAction(object o, CancellationToken ct)
            {
                callCounter++;
                return Task.Delay(10, ct);
            }
        
            bool MockCanExecute(object o)
            {
                return true;
            }
        
            var systemUnderTest = new BackgroundCommand(MockAction, MockCanExecute);
            
            int tokenCounter = -1;
            var cancellationTokenSources = new CancellationTokenSource[2];
            cancellationTokenSources[0] = new CancellationTokenSource();
            cancellationTokenSources[1] = new CancellationTokenSource();
            systemUnderTest.RegisterCancellationTokenFactory(() =>
            {
                tokenCounter++;
                return cancellationTokenSources[tokenCounter].Token;
            });

            await systemUnderTest.ExecuteAsync();
            cancellationTokenSources[0].Cancel();
            callCounter.Should().Be(1);
            await systemUnderTest.ExecuteAsync();
            cancellationTokenSources[1].Cancel();
            callCounter.Should().Be(2);
        }
    }
}