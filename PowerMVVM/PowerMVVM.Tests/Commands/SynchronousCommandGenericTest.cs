﻿using System;
using System.Threading.Tasks;
using FluentAssertions;
using NUnit.Framework;
using PowerMVVM.Commands;
using PowerMVVM.Commands.Testability;
using PowerMVVM.Tests.Commands.Mocks;
using PowerMVVM.Tests.Commands.TestHelper;

// ReSharper disable ObjectCreationAsStatement - Unit Tests
namespace PowerMVVM.Tests.Commands
{
    [TestFixture]
    public class SynchronousCommandGenericTest
    {
        [TestCase(true)]
        [TestCase(false)]
        public void CanExecute_ReturnsCorrectValue(bool canExecute)
        {
            var sut = new SynchronousCommandGeneric<object>(o => { }, o => canExecute);

            sut.CanExecute().Should().Be(canExecute);
        }

        private void DummyAction(object o) { }

        [Test]
        public void CanExecute_ExecutesAction_WithInvalidNullParameter_ThrowsException()
        {
            var sut = new SynchronousCommandGeneric<bool>(o => { }, o => true);

            sut.Invoking(s => s.CanExecute<object>(null))
                .Should()
                .Throw<InvalidCastException>();
        }

        [Test]
        public void CanExecute_ExecutesAction_WithInvalidTypedParameter_ThrowsException()
        {
            var sut = new SynchronousCommandGeneric<bool>(o => { }, o => true);

            sut.Invoking(s => s.CanExecute(new int()))
                .Should()
                .Throw<InvalidCastException>();
        }

        [Test]
        public void CanExecute_ExecutesPassedFunction()
        {
            var canExecuteCalled = false;
            var sut = new SynchronousCommandGeneric<object>(o => { }, o =>
            {
                canExecuteCalled = true;
                return true;
            });

            sut.CanExecute();
            canExecuteCalled.Should().BeTrue();
        }

        [Test]
        public void Constructor_ThrowsWhenActionIsNull()
        {
            Assert.Throws<ArgumentNullException>(() =>
                new SynchronousCommandGeneric<object>(null!, o => true));
        }

        [Test]
        public void Constructor_ThrowsWhenCanExecuteIsNull()
        {
            Assert.Throws<ArgumentNullException>(() =>
                new SynchronousCommandGeneric<object>(o => { }, null!, null!));
        }

        [Test]
        public async Task Execute_ExecutesAction()
        {
            var actionCalled = false;
            var sut = new SynchronousCommandGeneric<object>(o => actionCalled = true, o => true);

            await sut.ExecuteActionAsync();
            actionCalled.Should().BeTrue();
        }

        [Test]
        public void Execute_ExecutesAction_WithInvalidNullParameter_ThrowsException()
        {
            var sut = new SynchronousCommandGeneric<bool>(o => { }, o => true);

            sut.Awaiting(s => s.ExecuteActionAsync<object>(null))
                .Should()
                .Throw<InvalidCastException>();
        }

        [Test]
        public void Execute_ExecutesAction_WithInvalidTypedParameter_ThrowsException()
        {
            var sut = new SynchronousCommandGeneric<bool>(o => { }, o => true);

            sut.Awaiting(s => s.ExecuteActionAsync(new int()))
                .Should()
                .Throw<InvalidCastException>();
        }

        [Test]
        public async Task Execute_ExecutesAction_WithoutCanExecuteMethodSet()
        {
            var actionCalled = false;
            var sut = new SynchronousCommandGeneric<object>(o => actionCalled = true);

            await sut.ExecuteActionAsync();
            actionCalled.Should().BeTrue();
        }

        [Test]
        public async Task Execute_ExecutesAction_WithParameter()
        {
            var dummyObject = new object();
            object passedObject = null!;
            var sut = new SynchronousCommandGeneric<object>(o => passedObject = o, o => true);

            await sut.ExecuteActionAsync(dummyObject);
            passedObject.Should().Be(dummyObject);
        }

        [Test]
        [UsesCommandSettings]
        public async Task LoggedCommandName_IsNameOfAction()
        {
            var mockLoggingProvider = new TestLoggingProvider();
            Command.LoggingProvider = mockLoggingProvider;
            var sut = new SynchronousCommandGeneric<object>(DummyAction, o => true, nameof(DummyAction));
            await sut.ExecuteAsync();

            mockLoggingProvider.AssertionProperty_CommandName.Should().Be(nameof(DummyAction));
        }
    }
}