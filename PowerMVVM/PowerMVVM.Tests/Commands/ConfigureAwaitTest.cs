﻿using NUnit.Framework;
using PowerMVVM.Commands;
using PowerMVVM.Tests.Commands.Mocks;
using PowerMVVM.Tests.Commands.TestHelper;

namespace PowerMVVM.Tests.Commands
{
    [TestFixture]
    public class ConfigureAwaitTest
    {
        [Test]
        public void WhenGlobalSettings_IsFalse_UsesDefault_IsFalse()
        {
            var command = new CommandMock();
            
            Assert.That(command.AssertionProperty_InternalConfigureAwait, Is.False);
        }

        [Test]
        [UsesCommandSettings]
        public void WhenGlobalSettings_IsTrue_UsesDefault_IsTrue()
        {
            Command.AlwaysContinueOnCapturedContext = true;
            var command = new CommandMock();
            
            Assert.That(command.AssertionProperty_InternalConfigureAwait, Is.True);
        }
        
        [Test]
        public void WhenGlobalSettings_IsFalse_AndConfigures_IsTrue()
        {
            var command = new CommandMock();
            command.ContinueOncapturedContext();
            
            Assert.That(command.AssertionProperty_InternalConfigureAwait, Is.True);
        }

        [Test]
        [UsesCommandSettings]
        public void WhenGlobalSettings_IsTrue_AndConfigured_IsTrue()
        {
            Command.AlwaysContinueOnCapturedContext = true;
            var command = new CommandMock();
            command.ContinueOncapturedContext();
            
            Assert.That(command.AssertionProperty_InternalConfigureAwait, Is.True);
        }
    }
}