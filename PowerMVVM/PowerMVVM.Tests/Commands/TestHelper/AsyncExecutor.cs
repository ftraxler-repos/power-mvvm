﻿using System.Threading.Tasks;
using PowerMVVM.Commands.Testability;

namespace PowerMVVM.Tests.Commands.TestHelper
{
    public static class AsyncExecutor
    {
        public static Task ExecuteAsync(this ICommandExecutor command, object parameter = null)
        {
            return command.ExecuteAsync(parameter);
        }
        
        public static Task ExecuteAsync(this IBackgroundCommandExecutor command, object parameter = null)
        {
            return command.ExecuteAsync(parameter);
        }
    }
}