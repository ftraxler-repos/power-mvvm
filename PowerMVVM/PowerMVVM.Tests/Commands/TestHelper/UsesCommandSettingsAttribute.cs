﻿using NUnit.Framework;
using NUnit.Framework.Interfaces;
using PowerMVVM.Commands;
using PowerMVVM.Commands.CommandSettings;

namespace PowerMVVM.Tests.Commands.TestHelper
{
    public class UsesCommandSettingsAttribute : TestActionAttribute
    {
        /// <inheritdoc />
        public override void AfterTest(ITest test)
        {
            Command.LoggingProvider = new DefaultLoggingProvider();
            Command.StatusProvider = new NullStatusProvider();
            Command.AlwaysContinueOnCapturedContext = false;
        }
    }
}