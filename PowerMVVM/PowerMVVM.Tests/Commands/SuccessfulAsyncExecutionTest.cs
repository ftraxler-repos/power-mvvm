﻿using System;
using System.Threading.Tasks;
using FluentAssertions;
using NSubstitute;
using NUnit.Framework;
using PowerMVVM.Commands;
using PowerMVVM.Commands.CommandSettings;
using PowerMVVM.Tests.Commands.Mocks;
using PowerMVVM.Tests.Commands.TestHelper;

namespace PowerMVVM.Tests.Commands
{
    [TestFixture]
    [UsesCommandSettings]
    public class SuccessfulAsyncExecutionTest
    {
        public CommandMock Mock { get; private set;}

        public ILoggingProvider MockLoggingProvider { get; private set; }

        public AStatusProvider MockStatusProvider { get; private set;}

        public ACommand SystemUnderTest { get; private set;}

        [SetUp]
        public void SetUp()
        {
            MockLoggingProvider = Substitute.For<ILoggingProvider>();
            Command.LoggingProvider = MockLoggingProvider;

            MockStatusProvider = Substitute.For<AStatusProvider>();
            Command.StatusProvider = MockStatusProvider;

            SystemUnderTest = Mock = new CommandMock
            {
                TestProperty_ExecutionTask = Task.CompletedTask
            };
        }
        
        [Test]
        public async Task ExecuteAsync_CallsContinuationAction()
        {
            var continuationActionCalled = false;
            SystemUnderTest.ContinueWith(() => continuationActionCalled = true);
            await SystemUnderTest.ExecuteAsync();

            continuationActionCalled.Should().BeTrue();
        }

        [Test]
        public async Task ExecuteAsync_CallsLoadingIndicator_WithArgumentFalse()
        {
            await SystemUnderTest.ExecuteAsync();

            await MockStatusProvider.Received().ShowLoadingIndicatorAsync(false);
        }

        [Test]
        public async Task ExecuteAsync_CallsLoadingIndicator_WithArgumentTrue_WhenConfigured()
        {
            SystemUnderTest.UseLoadingIndicator();
            await SystemUnderTest.ExecuteAsync();

            await MockStatusProvider.Received().ShowLoadingIndicatorAsync(true);
        }
        
        [Test]
        public async Task ExecuteAsync_HidesLoadingIndicatorWhenFinished_WithArgumentFalse()
        {
            await SystemUnderTest.ExecuteAsync();

            await MockStatusProvider.Received().HideLoadingIndicatorAsync(false);
        }

        [Test]
        public async Task ExecuteAsync_HidesLoadingIndicatorWhenFinished_WithArgumentTrue_WhenConfigured()
        {
            SystemUnderTest.UseLoadingIndicator();
            await SystemUnderTest.ExecuteAsync();

            await MockStatusProvider.Received().HideLoadingIndicatorAsync(true);
        }

        [Test]
        public async Task ExecuteAsync_Logs_WhenCalled()
        {
            await SystemUnderTest.ExecuteAsync();

            MockLoggingProvider.ReceivedWithAnyArgs().OnCall(Arg.Any<string>(), Arg.Is<object>(o => o == null));
        }

        [Test]
        public async Task ExecuteAsync_Logs_WhenFinished()
        {
            await SystemUnderTest.ExecuteAsync();

            MockLoggingProvider.ReceivedWithAnyArgs().OnCallCompleted(Arg.Any<string>(), Arg.Is<object>(o => o == null));
        }

        [Test]
        public async Task ExecuteAsync_LogsNothing_OnException()
        {
            await SystemUnderTest.ExecuteAsync();

            MockLoggingProvider.DidNotReceiveWithAnyArgs().OnException(Arg.Any<Exception>());
        }
    }
}