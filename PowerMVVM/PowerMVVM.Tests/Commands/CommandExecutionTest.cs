﻿using System.Threading.Tasks;
using System.Windows.Input;
using FluentAssertions;
using NSubstitute;
using NUnit.Framework;
using PowerMVVM.Commands;
using PowerMVVM.Commands.CommandSettings;
using PowerMVVM.Tests.Commands.Mocks;
using PowerMVVM.Tests.Commands.TestHelper;

namespace PowerMVVM.Tests.Commands
{
    [TestFixture]
    [UsesCommandSettings]
    public class CommandExecutionTest
    {
        public CommandMock Mock { get; private set;}

        public ILoggingProvider MockLoggingProvider { get; private set; }

        public AStatusProvider MockStatusProvider { get; private set;}

        public ACommand SystemUnderTest { get; private set;}

        [SetUp]
        public void SetUp()
        {
            MockLoggingProvider = Substitute.For<ILoggingProvider>();
            Command.LoggingProvider = MockLoggingProvider;

            MockStatusProvider = Substitute.For<AStatusProvider>();
            Command.StatusProvider = MockStatusProvider;

            SystemUnderTest = Mock = new CommandMock
            {
                TestProperty_ExecutionTask = Task.CompletedTask
            };
            
            _icommand = SystemUnderTest;
        }

        private ICommand _icommand;

        [Test]
        public void CanExecute_CallsCanExecuteInternal()
        {
            _icommand.CanExecute(null);

            Mock.AssertionProperty_CanExecuteInternalCalled.Should().Be(1);
        }

        [Test]
        public void Execute_ExecutesAsync()
        {
            _icommand.Execute(null);

            // check logging provider to assure it is not the plain call to the prototype method
            MockLoggingProvider.Received().OnCall(Arg.Any<string>(), Arg.Any<object>());
            Mock.AssertionProperty_ExecuteInternalCalled.Should().Be(1);
            MockLoggingProvider.Received().OnCallCompleted(Arg.Any<string>(), Arg.Any<object>());
        }
    }
}