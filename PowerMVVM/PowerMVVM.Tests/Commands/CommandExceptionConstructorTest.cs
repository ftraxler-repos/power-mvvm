﻿using System;
using FluentAssertions;
using NUnit.Framework;
using PowerMVVM.Commands;

namespace PowerMVVM.Tests.Commands
{
    [TestFixture]
    public class CommandExceptionConstructorTest
    {
        [Test]
        public void PassingNullAsException_Throws()
        {
            this.Invoking(x => new CommandException(null!, null))
                .Should()
                .Throw<ArgumentNullException>();
        }
        
        [Test]
        public void PassingNullAsSynchronisationContext_DoesNotThrow()
        {
            this.Invoking(x => new CommandException(new Exception(), null))
                .Should()
                .NotThrow();
        }
    }
}