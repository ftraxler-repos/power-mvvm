﻿using System.Threading.Tasks;
using FluentAssertions;
using NUnit.Framework;
using PowerMVVM.Commands;
using PowerMVVM.Tests.Commands.Mocks;
using PowerMVVM.Tests.Commands.TestHelper;

namespace PowerMVVM.Tests.Commands
{
    [TestFixture]
    public class DoubleExecutionPreventionTest
    {
        protected CommandMock Mock { get; private set; }

        protected ACommand SystemUnderTest { get; private set; }

        [SetUp]
        public void SetUp()
        {
            SystemUnderTest = Mock = new CommandMock();
            Mock.TestProperty_ExecutionTask = Task.Delay(25);
        }

        [Test]
        public async Task DoesNotPreventDoubleExecution_WhenConfigured()
        {
            SystemUnderTest.AllowMultipleExecution();

            await Task.Factory.StartNew(() => SystemUnderTest.ExecuteAsync());
            await Task.Factory.StartNew(() => SystemUnderTest.ExecuteAsync());

            await Task.Delay(25);

            Mock.AssertionProperty_ExecuteInternalCalled.Should().Be(2);
        }

        [Test]
        public async Task PreventsDoubleExecution()
        {
            await Task.Factory.StartNew(() => SystemUnderTest.ExecuteAsync());
            await Task.Factory.StartNew(() => SystemUnderTest.ExecuteAsync());

            await Task.Delay(25);

            Mock.AssertionProperty_ExecuteInternalCalled.Should().Be(1);
        }
    }
}