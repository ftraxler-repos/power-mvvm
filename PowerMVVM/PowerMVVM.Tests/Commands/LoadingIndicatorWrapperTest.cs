﻿using System.Threading.Tasks;
using NUnit.Framework;
using PowerMVVM.Commands;

namespace PowerMVVM.Tests.Commands
{
    [TestFixture]
    public class LoadingIndicatorWrapperTest
    {
        private LoadingIndicatorWrapper _systemUnderTest;
        private bool _showTaskCalled, _hideTaskCalled;

        [SetUp]
        public void Setup()
        {
            _systemUnderTest = new LoadingIndicatorWrapper(() =>
                {
                    _showTaskCalled = true;
                    return Task.CompletedTask;
                },
                () =>
                {
                    _hideTaskCalled = true;
                    return Task.CompletedTask;
                });
        }

        [Test]
        public void CreateWithoutShowTask_Throws()
        {
            Assert.That(() => new LoadingIndicatorWrapper(null, () => Task.CompletedTask), Throws.ArgumentNullException);
        }
        
        [Test]
        public void CreateWithoutHideTask_Throws()
        {
            Assert.That(() => new LoadingIndicatorWrapper(() => Task.CompletedTask, null), Throws.ArgumentNullException);
        }

        [Test]
        public async Task Show_CallsShowFunction()
        {
            await _systemUnderTest.ShowLoadingAsync(string.Empty);
            Assume.That(_hideTaskCalled, Is.False);
            Assert.That(_showTaskCalled, Is.True);
        }
        
        [Test]
        public async Task Hide_CallsHideFunction()
        {
            await _systemUnderTest.HideLoadingAsync();
            Assume.That(_showTaskCalled, Is.False);
            Assert.That(_hideTaskCalled, Is.True);
        }
    }
}