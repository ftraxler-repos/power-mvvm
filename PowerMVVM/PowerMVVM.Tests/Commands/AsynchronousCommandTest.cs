﻿using System;
using System.Threading.Tasks;
using FluentAssertions;
using NUnit.Framework;
using PowerMVVM.Commands;
using PowerMVVM.Commands.Testability;
using PowerMVVM.Tests.Commands.Mocks;
using PowerMVVM.Tests.Commands.TestHelper;
// ReSharper disable ObjectCreationAsStatement - Unit Tests

namespace PowerMVVM.Tests.Commands
{
    [TestFixture]
    public class AsynchronousCommandTest
    {
        [TestCase(true)]
        [TestCase(false)]
        public void CanExecute_ReturnsCorrectValue(bool canExecute)
        {
            var sut = new AsynchronousCommand(() => Task.Delay(1), () => canExecute);

            sut.CanExecute().Should().Be(canExecute);
        }

        private Task DummyAction()
        {
            return Task.Delay(1);
        }

        [Test]
        public void CanExecute_ExecutesPassedFunction()
        {
            var canExecuteCalled = false;
            var sut = new AsynchronousCommand(() => Task.Delay(1), () =>
            {
                canExecuteCalled = true;
                return true;
            });

            sut.CanExecute();
            canExecuteCalled.Should().BeTrue();
        }

        [Test]
        public void Constructor_ThrowsWhenActionIsNull()
        {
            Assert.Throws<ArgumentNullException>(() =>
                new AsynchronousCommand(null!, () => true));
        }

        [Test]
        public void Constructor_ThrowsWhenCanExecuteIsNull()
        {
            Assert.Throws<ArgumentNullException>(() =>
                new AsynchronousCommand(() => Task.Delay(1), null!, null!));
        }

        [Test]
        public async Task Execute_ExecutesAction()
        {
            var actionCalled = false;
            var sut = new AsynchronousCommand(async () =>
            {
                actionCalled = true;
                await Task.Delay(1);
            }, () => true);

            await sut.ExecuteActionAsync();
            actionCalled.Should().BeTrue();
        }

        [Test]
        public async Task Execute_ExecutesAction_WithoutCanExecuteMethodSet()
        {
            var actionCalled = false;
            var sut = new AsynchronousCommand(async () =>
            {
                actionCalled = true;
                await Task.Delay(1);
            });

            await sut.ExecuteActionAsync();
            actionCalled.Should().BeTrue();
        }

        [Test]
        public void Execute_ExecutesAction_WithoutParameter()
        {
            var sut = new AsynchronousCommand(() => Task.Delay(1), () => true);

            sut.Invoking(async s => await sut.ExecuteActionAsync((object)null)).Should().NotThrow();
        }

        [Test]
        [UsesCommandSettings]
        public async Task LoggedCommandName_IsNameOfAction()
        {
            var mockLoggingProvider = new TestLoggingProvider();
            Command.LoggingProvider = mockLoggingProvider;
            var sut = new AsynchronousCommand(DummyAction, () => true, nameof(DummyAction));
            await sut.ExecuteAsync();

            mockLoggingProvider.AssertionProperty_CommandName.Should().Be(nameof(DummyAction));
        }
    }
}