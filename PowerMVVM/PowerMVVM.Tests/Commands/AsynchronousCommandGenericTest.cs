﻿using System;
using System.Threading.Tasks;
using FluentAssertions;
using NUnit.Framework;
using PowerMVVM.Commands;
using PowerMVVM.Commands.Testability;
using PowerMVVM.Tests.Commands.Mocks;
using PowerMVVM.Tests.Commands.TestHelper;

// ReSharper disable ObjectCreationAsStatement - Unit Tests
namespace PowerMVVM.Tests.Commands
{
    [TestFixture]
    public class AsynchronousCommandGenericTest
    {
        [TestCase(true)]
        [TestCase(false)]
        public void CanExecute_ReturnsCorrectValue(bool canExecute)
        {
            var sut = new AsynchronousCommandGeneric<object>(o => Task.Delay(1), o => canExecute);

            sut.CanExecute().Should().Be(canExecute);
        }

        private Task DummyAction(object o)
        {
            return Task.Delay(1);
        }

        [Test]
        public void CanExecute_ExecutesAction_WithInvalidNullParameter_ThrowsException()
        {
            var sut = new AsynchronousCommandGeneric<bool>(async o => { await Task.Delay(1); }, o => true);

            sut.Invoking(s => s.CanExecute<object>(null))
                .Should()
                .Throw<InvalidCastException>();
        }

        [Test]
        public void CanExecute_ExecutesAction_WithInvalidTypedParameter_ThrowsException()
        {
            var sut = new AsynchronousCommandGeneric<bool>(async o => { await Task.Delay(1); }, o => true);

            sut.Invoking(s => s.CanExecute(new int()))
                .Should()
                .Throw<InvalidCastException>();
        }

        [Test]
        public void CanExecute_ExecutesPassedFunction()
        {
            var canExecuteCalled = false;
            var sut = new AsynchronousCommandGeneric<object>(o => Task.Delay(1), o =>
            {
                canExecuteCalled = true;
                return true;
            });

            sut.CanExecute();
            canExecuteCalled.Should().BeTrue();
        }

        [Test]
        public void Constructor_ThrowsWhenActionIsNull()
        {
            Assert.Throws<ArgumentNullException>(() =>
                new AsynchronousCommandGeneric<object>(null!, o => true));
        }

        [Test]
        public void Constructor_ThrowsWhenCanExecuteIsNull()
        {
            Assert.Throws<ArgumentNullException>(() =>
                new AsynchronousCommandGeneric<object>(o => Task.Delay(1), null!, null!));
        }

        [Test]
        public async Task Execute_ExecutesAction()
        {
            var actionCalled = false;
            var sut = new AsynchronousCommandGeneric<object>(async o =>
            {
                actionCalled = true;
                await Task.Delay(1);
            }, o => true);

            await sut.ExecuteActionAsync();
            actionCalled.Should().BeTrue();
        }

        [Test]
        public void Execute_ExecutesAction_WithInvalidNullParameter_ThrowsException()
        {
            var sut = new AsynchronousCommandGeneric<bool>(async o => { await Task.Delay(1); }, o => true);

            sut.Awaiting(s => s.ExecuteActionAsync<object>(null))
                .Should()
                .Throw<InvalidCastException>();
        }

        [Test]
        public void Execute_ExecutesAction_WithInvalidTypedParameter_ThrowsException()
        {
            var sut = new AsynchronousCommandGeneric<bool>(async o => { await Task.Delay(1); }, o => true);

            sut.Awaiting(s => s.ExecuteActionAsync(new int()))
                .Should()
                .Throw<InvalidCastException>();
        }

        [Test]
        public async Task Execute_ExecutesAction_WithoutCanExecuteMethodSet()
        {
            var actionCalled = false;
            var sut = new AsynchronousCommandGeneric<object>(async o =>
            {
                actionCalled = true;
                await Task.Delay(1);
            });

            await sut.ExecuteActionAsync();
            actionCalled.Should().BeTrue();
        }

        [Test]
        public async Task Execute_ExecutesAction_WithParameter()
        {
            var dummyObject = new object();
            object passedObject = null!;
            var sut = new AsynchronousCommandGeneric<object>(async o =>
            {
                passedObject = o;
                await Task.Delay(1);
            }, o => true);

            await sut.ExecuteActionAsync(dummyObject);
            passedObject.Should().Be(dummyObject);
        }

        [Test]
        [UsesCommandSettings]
        public async Task LoggedCommandName_IsNameOfAction()
        {
            var mockLoggingProvider = new TestLoggingProvider();
            Command.LoggingProvider = mockLoggingProvider;
            var sut = new AsynchronousCommandGeneric<object>(DummyAction, o => true, nameof(DummyAction));
            await sut.ExecuteAsync();

            mockLoggingProvider.AssertionProperty_CommandName.Should().Be(nameof(DummyAction));
        }
    }
}