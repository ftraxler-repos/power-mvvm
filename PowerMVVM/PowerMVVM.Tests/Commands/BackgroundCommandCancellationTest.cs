﻿using System.Threading;
using System.Threading.Tasks;
using FluentAssertions;
using NSubstitute;
using NUnit.Framework;
using PowerMVVM.Commands;
using PowerMVVM.Commands.CommandSettings;
using PowerMVVM.Tests.Commands.TestHelper;

namespace PowerMVVM.Tests.Commands
{
    [TestFixture]
    [UsesCommandSettings]
    public class BackgroundCommandCancellationTest
    {
        private BackgroundCommand _systemUnderTest;
        private CancellationTokenSource _cancellationTokenSource;
        private ILoggingProvider _mockLoggigngProvider;

        [SetUp]
        public void SetUp()
        {
            _mockLoggigngProvider = Substitute.For<ILoggingProvider>();
            Command.LoggingProvider = _mockLoggigngProvider;
            
            Task MockAction(object o, CancellationToken ct)
            {
                return Task.Delay(10, ct);
            }

            bool MockCanExecute(object o)
            {
                return true;
            }

            _cancellationTokenSource = new CancellationTokenSource();
            _systemUnderTest = new BackgroundCommand(MockAction, MockCanExecute);
            _systemUnderTest.RegisterCancellationTokenFactory(() => _cancellationTokenSource.Token);
        }

        [Test]
        public async Task CancelCommand_InvokesCancelledHandler()
        {
            var cancellationHandlerCalled = false;
            _systemUnderTest.SetCancellationHandler(() => cancellationHandlerCalled = true);
            await _systemUnderTest.ExecuteAsync();
            _cancellationTokenSource.Cancel();

            cancellationHandlerCalled.Should().BeTrue();
        }
        
        [Test]
        public async Task CancelCommand_CallsLog()
        {
            await _systemUnderTest.ExecuteAsync();
            _cancellationTokenSource.Cancel();

            _mockLoggigngProvider.Received().OnException(Arg.Any<TaskCanceledException>());
        }
        
        [Test]
        public async Task CancelCommand_BeforeCall_CallsLog()
        {
            _cancellationTokenSource.Cancel();
            await _systemUnderTest.ExecuteAsync();

            _mockLoggigngProvider.Received().OnException(Arg.Any<TaskCanceledException>());
        }
        
        [Test]
        public async Task CancelCommand_BeforeCall_InvokesCancelledHandler()
        {
            var cancellationHandlerCalled = false;
            _systemUnderTest.SetCancellationHandler(() => cancellationHandlerCalled = true);
            
            _cancellationTokenSource.Cancel();
            await _systemUnderTest.ExecuteAsync();

            cancellationHandlerCalled.Should().BeTrue();
        }
    }
}