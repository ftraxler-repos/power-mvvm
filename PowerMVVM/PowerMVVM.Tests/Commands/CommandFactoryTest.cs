﻿using System.Threading.Tasks;
using System.Windows.Input;
using FluentAssertions;
using NSubstitute;
using NUnit.Framework;
using PowerMVVM.Commands;
using PowerMVVM.Commands.CommandSettings;
using PowerMVVM.Commands.Testability;
using PowerMVVM.Tests.Commands.TestHelper;

namespace PowerMVVM.Tests.Commands
{
    [TestFixture]
    public class CommandFactoryTest
    {
        private void MockAction() { }

        private void MockAction(object parameter) { }

        private Task MockTask()
        {
            return Task.CompletedTask;
        }

        private Task MockTask(object parameter)
        {
            return Task.CompletedTask;
        }

        private bool CanExecuteFunction()
        {
            return true;
        }

        private bool CanExecuteFunction(object parameter)
        {
            return true;
        }

        [Test]
        public void Create_AsynchronousCommand_FromAction()
        {
            var command = Command.Create(MockTask);
            command.Should().BeOfType<AsynchronousCommand>();
            command.CanExecute().Should().BeTrue();
        }

        [Test]
        public void Create_AsynchronousCommand_FromActionWithCanExecute()
        {
            var command = Command.Create(MockTask, CanExecuteFunction);
            command.Should().BeOfType<AsynchronousCommand>();
        }

        [Test]
        public void Create_SynchronousCommand_FromAction()
        {
            var command = Command.Create(MockAction);
            command.Should().BeOfType<SynchronousCommand>();
            command.CanExecute().Should().BeTrue();
        }

        [Test]
        public void Create_SynchronousCommand_FromActionWithCanExecute()
        {
            var command = Command.Create(MockAction, CanExecuteFunction);
            command.Should().BeOfType<SynchronousCommand>();
        }

        [Test]
        public void Create_WrappedCommand_FromExistingCommand()
        {
            var mockCommand = Substitute.For<ICommand>();
            var command = Command.Create(mockCommand);
            command.Should().BeOfType<WrappedCommand>();
        }

        [Test]
        public void CreateT_AsynchronousCommand_FromAction()
        {
            var command = Command.Create<object>(MockTask);
            command.Should().BeOfType<AsynchronousCommandGeneric<object>>();
            command.CanExecute().Should().BeTrue();
        }

        [Test]
        public void CreateT_AsynchronousCommand_FromActionWithCanExecute()
        {
            var command = Command.Create<object>(MockTask, CanExecuteFunction);
            command.Should().BeOfType<AsynchronousCommandGeneric<object>>();
        }

        [Test]
        public void CreateT_SynchronousCommand_FromAction()
        {
            var command = Command.Create<object>(MockAction);
            command.Should().BeOfType<SynchronousCommandGeneric<object>>();
            command.CanExecute().Should().BeTrue();
        }

        [Test]
        public void CreateT_SynchronousCommand_FromActionWithCanExecute()
        {
            var command = Command.Create<object>(MockAction, CanExecuteFunction);
            command.Should().BeOfType<SynchronousCommandGeneric<object>>();
        }

        [Test]
        [UsesCommandSettings]
        public void LoggingProvider_IsNullLoggingProvider_WhenSetToNull()
        {
            Command.LoggingProvider = Substitute.For<ILoggingProvider>();
            Command.LoggingProvider = null!;
            Command.LoggingProvider.Should().BeOfType<NullLoggingProvider>();
        }

        [Test]
        [UsesCommandSettings]
        public void StatusProvider_IsNullStatusProvider_WhenSetToNull()
        {
            Command.StatusProvider = Substitute.For<AStatusProvider>();
            Command.StatusProvider = null!;
            Command.StatusProvider.Should().BeOfType<NullStatusProvider>();
        }
    }
}