﻿using NUnit.Framework;
using PowerMVVM.Commands;

namespace PowerMVVM.Tests.Commands
{
    [TestFixture]
    public class AutoResetFlagTest
    {
        [Test]
        public void CheckingIsSignalled_DoesNotResetFlag()
        {
            var sut = new AutoResetFlag();
            sut.Signal();
            Assume.That(sut.IsSignalled, Is.True);
            Assert.That(sut.IsSignalled, Is.True);
        }

        [Test]
        public void CheckingSignalledstatus_Resets_IsSignalled()
        {
            var sut = new AutoResetFlag();
            sut.Signal();
            sut.CheckSignalled();
            Assert.That(sut.IsSignalled, Is.False);
        }

        [Test]
        public void CheckingSignalledStatus_ShouldResetSignallesToFalse_WhenPreviouslySignalled()
        {
            var sut = new AutoResetFlag();
            sut.Signal();
            Assume.That(sut.CheckSignalled(), Is.True);
            Assert.That(sut.CheckSignalled(), Is.False);
        }

        [Test]
        public void IsSignalled_IsFalseWhenNewlyCreated()
        {
            var sut = new AutoResetFlag();
            Assert.That(sut.IsSignalled, Is.False);
        }

        [Test]
        public void IsSignalled_IsTrue_WhenSignalled()
        {
            var sut = new AutoResetFlag();
            sut.Signal();
            Assert.That(sut.IsSignalled, Is.True);
        }

        [Test]
        public void NewlyCreated_ShouldNotSignal()
        {
            var sut = new AutoResetFlag();
            Assert.That(sut.CheckSignalled(), Is.False);
        }

        [Test]
        public void Signal_SetsSignalledToTrue()
        {
            var sut = new AutoResetFlag();
            sut.Signal();
            Assert.That(sut.CheckSignalled(), Is.True);
        }
    }
}