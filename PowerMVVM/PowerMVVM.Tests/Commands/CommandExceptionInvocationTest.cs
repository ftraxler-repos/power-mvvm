﻿using System;
using FluentAssertions;
using NUnit.Framework;
using PowerMVVM.Commands;

namespace PowerMVVM.Tests.Commands
{
    [TestFixture]
    public class CommandExceptionInvocationTest
    {
        [Test]
        public void InvocatorInvokesExceptionPassed()
        {
            ICommandExceptionInvocator systemUnderTest = new CommandException(new NullReferenceException(), null);
            systemUnderTest.Invoking(sut => sut.Invoke())
                           .Should()
                           .ThrowExactly<NullReferenceException>();
        }
    }
}